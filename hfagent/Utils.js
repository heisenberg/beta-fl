exports.makeId = function (length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

exports.prettyJSONString = function (inputString) {
    return JSON.stringify(JSON.parse(inputString), null, 2);
}

exports.retrieveData = async function (array) {
    if (array === ''){
        return array;
    }
    return JSON.stringify(array);
}