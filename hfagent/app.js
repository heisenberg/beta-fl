'use strict';
require('dotenv').config()

var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var util = require('util');
var app = express();
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var bearerToken = require('express-bearer-token');
var cors = require('cors');
const fileUpload = require('express-fileupload');
var { create } = require('ipfs-http-client')

const { Gateway, Wallets } = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const path = require('path');
const { buildCAClient, registerAndEnrollUser, enrollUser, checkUserExists} = require('./CAUtil.js');
const { buildCCPSupplyChainActors, buildCCPConsumers, buildCCPRetailers, buildCCPWholesalers, buildCCPPlatformOperator, buildCCPAiServiceProviders, buildCCPExternalParties, buildWallet, getConnectionProfileAndWalletPath } = require('./AppUtil.js');
const {prettyJSONString} = require("./Utils.js");
const {retrieveData} = require("./Utils.js");
const fs = require("fs");

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// SET CONFIGURATONS ////////////////////////////
///////////////////////////////////////////////////////////////////////////////
app.options('*', cors());
app.use(fileUpload({
    createParentPath: true
}));
app.use(cors());
app.use(bodyParser.json());
app.set('secret', process.env.SERVER_SECRET);
app.use(expressJWT({
    secret: process.env.SERVER_SECRET,
    algorithms: ['HS256']
}).unless({
    path: ['/users/register','/local-model/event','/global-model/event','/website/invoke']
}));

app.use(bearerToken());

app.use((req, res, next) => {
    console.log(' ------>>>>>> new request for %s', req.originalUrl);


    if (req.originalUrl === '/users/register' || req.originalUrl === '/local-model/event' || req.originalUrl === '/global-model/event'
        || req.originalUrl === '/website/invoke') {
        return next();
    }
    // if(req.name === 'UnauthorizedError') {
    //     res.status(error.status).send({message:error.message});
    //     console.log(error);
    //     return;
    // }


    var token = req.token;
    jwt.verify(token, app.get('secret'), { algorithms: ['HS256']}, function(err, decoded) {
        if (err) {
            res.status(403).send({
                success: false,
                message: 'Failed to authenticate token.'
            });
            return;
        } else {
            req.username = decoded.username;
            req.orgname = decoded.orgName;
            console.log(util.format('Decoded from JWT token: username - %s, orgname - %s', decoded.username, decoded.orgName));
            return next();
        }
    });
});

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// START SERVER /////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
var server = http.createServer(app).listen(process.env.PORT, function() {});
console.log('****************** SERVER STARTED ************************');
server.timeout = 240000;

function getErrorMessage(field) {
    return {
        success: false,
        message: field + ' field is missing or Invalid in the request'
    };
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////// REST ENDPOINTS START HERE ///////////////////////////
///////////////////////////////////////////////////////////////////////////////

// Register and enrol user
app.post('/users/register', async function(req, res) {
    var username = req.body.username;
    var orgName = req.body.orgName;
    var attrs = req.body.attrs;
    var password = req.body.password;

    console.log('User name : ' + username);
    console.log('Org name  : ' + orgName);
    console.log('Attrs  : ' + attrs);
    console.log('password  : ' + password);

    if (!username) {
        res.status(400).json(getErrorMessage('\'username\''));
        return;
    }
    if (!orgName) {
        res.status(400).json(getErrorMessage('\'orgName\''));
        return;
    }
    if (!password) {
        res.status(400).json(getErrorMessage('\'password\''));
        return;
    }

    var token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + parseInt(process.env.JWT_EXPIRATION),
        username: username,
        orgName: orgName,
        password: password
    }, app.get('secret'));


    let conAndWallet = getConnectionProfileAndWalletPath(orgName)
    let ccp = conAndWallet.ccp
    let walletPath = conAndWallet.walletPath

    const caHostName = Object.keys(ccp.certificateAuthorities)[0]
    const caClient = buildCAClient(FabricCAServices, ccp, caHostName);

    const walletObj = await buildWallet(Wallets, walletPath);

    const orgMspId = `${orgName}MSP`
    let exists = await checkUserExists(walletObj, username)
    if (!(exists)){
        let success = await registerAndEnrollUser(caClient, walletObj, orgMspId, username, password, attrs);
        if (!(success))
            res.status(400).json({success: false, message: 'Failed to register&enroll enroll user.'});
    }
    let response = {}
    response.username = username
    response.orgName = orgName
    response.token = token
    response.success = true
    res.status(200).json(response);
});


// Invoke transaction on chaincode on target peers
app.post('/website/invoke', async function(req, res) {
    console.log('==================== INVOKE ON CHAINCODE ==================');
    var orgName = req.body.orgName
    var username = req.body.username
    var chaincodeName = req.body.chaincodeName;
    var channelName = req.body.channelName;
    var fcn = req.body.fcn;
    var args = req.body.args;

    console.log('orgName  : ' + orgName);
    console.log('username  : ' + username);
    console.log('channelName  : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn  : ' + fcn);
    console.log('args  : ' + args);

    if (!chaincodeName) {
        res.status(400).json(getErrorMessage('\'chaincodeName\''));
        return;
    }
    if (!channelName) {
        res.status(400).json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!fcn) {
        res.status(400).json(getErrorMessage('\'fcn\''));
        return;
    }
    if (!args) {
        res.status(400).json(getErrorMessage('\'args\''));
        return;
    }

    const gateway = new Gateway();

    try {
        let conAndWallet = getConnectionProfileAndWalletPath(orgName)
        let ccp = conAndWallet.ccp
        let walletPath = conAndWallet.walletPath

        const walletObj = await buildWallet(Wallets, walletPath);

        const gateway = new Gateway();
        const userIdentity = await walletObj.get(username);
        await gateway.connect(ccp, {
            walletObj,
            identity: userIdentity,
            discovery: { enabled: true, asLocalhost: true }
        });

            const network = await gateway.getNetwork(channelName);
        const contract = network.getContract(chaincodeName);

        try {
            let txExecutionResult = await contract.submitTransaction(fcn, ...args);
            console.log(`*** Successful execution: ${prettyJSONString(txExecutionResult.toString())}`);
            res.status(200).json({success:true, result:JSON.parse(txExecutionResult.toString())});
            return

        } catch (error) {
            console.log(`*** Failed execution error: \n ${error}`);
            res.status(400).json({success:false, results:error});
            return
        }
    } finally {
        gateway.disconnect();
    }
    res.status(400).json({success:false, results:'Error'});
});

// Invoke transaction on chaincode on target peers
app.post('/channels/:channelName/chaincodes/:chaincodeName/invoke', async function(req, res) {
    console.log('==================== INVOKE ON CHAINCODE ==================');
    var orgName = req.orgname
    var username = req.username
    var chaincodeName = req.params.chaincodeName;
    var channelName = req.params.channelName;
    var fcn = req.body.fcn;
    var args = req.body.args;

    console.log('orgName  : ' + orgName);
    console.log('username  : ' + username);
    console.log('channelName  : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn  : ' + fcn);
    console.log('args  : ' + args);

    if (!chaincodeName) {
        res.status(400).json(getErrorMessage('\'chaincodeName\''));
        return;
    }
    if (!channelName) {
        res.status(400).json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!fcn) {
        res.status(400).json(getErrorMessage('\'fcn\''));
        return;
    }
    if (!args) {
        res.status(400).json(getErrorMessage('\'args\''));
        return;
    }

    const gateway = new Gateway();

    try {
        let conAndWallet = getConnectionProfileAndWalletPath(orgName)
        let ccp = conAndWallet.ccp
        let walletPath = conAndWallet.walletPath

        const walletObj = await buildWallet(Wallets, walletPath);

        const gateway = new Gateway();
        const userIdentity = await walletObj.get(username);
        await gateway.connect(ccp, {
            walletObj,
            identity: userIdentity,
            discovery: { enabled: true, asLocalhost: true }
        });

        const network = await gateway.getNetwork(channelName);
        const contract = network.getContract(chaincodeName);

        try {
            let txExecutionResult = await contract.submitTransaction(fcn, ...args);
            console.log(`*** Successful execution: ${prettyJSONString(txExecutionResult.toString())}`);
            res.status(200).json({success:true, result:JSON.parse(txExecutionResult.toString())});
            return

        } catch (error) {
            console.log(`*** Failed execution error: \n ${error}`);
            res.status(400).json({success:false, results:error});
            return
        }
    } finally {
        gateway.disconnect();
    }
    res.status(400).json({success:false, results:'Error'});
});

// Query on chaincode on target peers
app.post('/channels/:channelName/chaincodes/:chaincodeName/query', async function(req, res) {
    console.log('==================== QUERY BY CHAINCODE ==================');
    var orgName = req.orgname
    var username = req.username
    var chaincodeName = req.params.chaincodeName;
    var channelName = req.params.channelName;
    let args = req.body.args;
    let fcn = req.body.fcn;

    console.log('orgName  : ' + orgName);
    console.log('username  : ' + username);
    console.log('channelName : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn : ' + fcn);
    console.log('args : ' + args);

    if (!chaincodeName) {
        res.status(400).json(getErrorMessage('\'chaincodeName\''));
        return;
    }
    if (!channelName) {
        res.status(400).json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!fcn) {
        res.status(400).json(getErrorMessage('\'fcn\''));
        return;
    }
    if (!args) {
        res.status(400).json(getErrorMessage('\'args\''));
        return;
    }

    const gateway = new Gateway();

    try {
        let conAndWallet = getConnectionProfileAndWalletPath(orgName)
        let ccp = conAndWallet.ccp
        let walletPath = conAndWallet.walletPath

        const walletObj = await buildWallet(Wallets, walletPath);

        const gateway = new Gateway();
        const userIdentity = await walletObj.get(username);
        await gateway.connect(ccp, {
            walletObj,
            identity: userIdentity,
            discovery: { enabled: true, asLocalhost: true }
        });

        const network = await gateway.getNetwork(channelName);

        const contract = network.getContract(chaincodeName);

        try {
            let txExecutionResult = await contract.evaluateTransaction(fcn, ...args);
            console.log(`*** Successful execution: ${prettyJSONString(txExecutionResult.toString())}`);
            res.status(200).json({success:true, result:JSON.parse(txExecutionResult.toString())});
            return
        } catch (error) {
            console.log(`*** Failed execution error: \n ${error}`);
            res.status(400).json({success:false, results:error});
            return
        }
    } finally {
        gateway.disconnect();
    }
    res.status(400).json({success:false, results:'Error'});
});

app.post('/ipfs/add', async function (req, res) {
    try {
        if(!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            let file = req.files.file;

            const client = create()
            const { cid } = await client.add(file.data)

            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: file.name,
                    mimetype: file.mimetype,
                    size: file.size,
                    cid: cid.toString()
                }
            });
        }
    } catch (err) {
        console.log(err)
        res.status(400).json({success:false, results:err});
    }
});


app.post('/ipfs/get', async function (req, res) {
    try {
        var cid = req.body.cid
        var ext = req.body.ext
        var name = 'test.' + ext

        console.log(cid, ext, name)

        const client = create()
        const result = await client.cat(cid)
        for await (const item of result) {
            console.log(item)
            fs.appendFileSync(name, item)
        }

        let fileBuffer = fs.readFileSync(name)
        console.log(fileBuffer)
        let str = fileBuffer.toString('base64')

        fs.unlinkSync(name)
        console.log('Temp file removed')

        res.send({
            status: true,
            data: {
                file: str,
            }
        });

    } catch (err) {
        console.log(err)
        res.status(400).json({success:false, results:err});
    }
});

app.post('/init-local-event/', async function(req, res) {
    console.log('==================== INVOKE ON CHAINCODE ==================');
    var orgName = req.orgname
    var username = req.username
    var roundNumber = req.body.roundNumber;
    var experimentID = req.body.experimentID;
    var modelLen = req.body.modelLen;

    var chaincodeName = 'eventsCC'
    var channelName = 'federetedlearning';
    var fcn = "eventUpdate"
    var args = [roundNumber.toString(), experimentID.toString(), modelLen.toString()]

    console.log('orgName  : ' + orgName);
    console.log('username  : ' + username);
    console.log('channelName  : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('args  : ' + args);

    if (!chaincodeName) {
        res.status(400).json(getErrorMessage('\'chaincodeName\''));
        return;
    }
    if (!channelName) {
        res.status(400).json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!fcn) {
        res.status(400).json(getErrorMessage('\'fcn\''));
        return;
    }
    if (!args) {
        res.status(400).json(getErrorMessage('\'args\''));
        return;
    }

    const gateway = new Gateway();

    try {
        let conAndWallet = getConnectionProfileAndWalletPath(orgName)
        let ccp = conAndWallet.ccp
        let walletPath = conAndWallet.walletPath

        const walletObj = await buildWallet(Wallets, walletPath);

        const gateway = new Gateway();
        const userIdentity = await walletObj.get(username);
        await gateway.connect(ccp, {
            walletObj,
            identity: userIdentity,
            discovery: { enabled: true, asLocalhost: true }
        });

        const network = await gateway.getNetwork(channelName);
        const contract = network.getContract(chaincodeName);

        try {
            let txExecutionResult = await contract.submitTransaction(fcn, ...args);
            console.log(`*** Successful execution: ${prettyJSONString(txExecutionResult.toString())}`);
            res.status(200).json({success:true, result:JSON.parse(txExecutionResult.toString())});
            return

        } catch (error) {
            console.log(`*** Failed execution error: \n ${error}`);
            res.status(400).json({success:false, results:error});
            return
        }
    } finally {
        gateway.disconnect();
    }
    res.status(400).json({success:false, results:'Error'});
});