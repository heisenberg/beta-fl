const {getConnectionProfileAndWalletPath, buildWallet} = require("./AppUtil.js");
const { Gateway, Wallets } = require('fabric-network');
const WebSocket = require('ws');
var { create } = require('ipfs-http-client')
const fs = require("fs");

var orgName = 'SupplyChainActors'
var username ='test'
var channel ='federetedlearning'
var chaincode = 'modelsManagement'
var data_out = []
var globalModel = ''
var queue = []
var counter = 0
var lastEvent = ""
var locked = false
var assets = []

const RED = '\x1b[31m\n';
const GREEN = '\x1b[32m\n';
const BLUE = '\x1b[34m';
const RESET = '\x1b[0m';

/**
 * Perform a sleep -- asynchronous wait
 * @param ms the time in milliseconds to sleep for
 */
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}


async function main() {
    try {
        console.log('Init logging')
        const wss = new WebSocket.Server({ port: 7070 });
        console.log('Web socket for local models connected 7070')

        wss.broadcast = function broadcast(data) {
            wss.clients.forEach(function each(client) {
                console.log('IT IS GETTING INSIDE CLIENTS');
                console.log(client);

                // The data is coming in correctly
                client.send(data);
            });
        };

        wss.on('connection', function (ws){
            console.log('New Client has connected')

            var id = setInterval(function() {
                if (data_out.length !== 0) {
                    console.log(data_out)
                    wss.broadcast(JSON.stringify(data_out));
                    sleep(500);
                    data_out = []
                }

            }, 500);

            ws.on('close', function() {
                console.log('Client has disconnected');
            });
        });


        const wssGlobal = new WebSocket.Server({ port: 7080 });
        console.log('Web socket for global model connected on port 7080')

        wssGlobal.broadcast = function broadcast(data) {
            wssGlobal.clients.forEach(function each(client) {
                console.log('IT IS GETTING INSIDE CLIENTS');
                console.log(client);

                // The data is coming in correctly
                client.send(data);
            });
        };

        wssGlobal.on('connection', function (ws){
            console.log('New Client has connected')

            var id = setInterval(function() {
                if (globalModel !== '') {
                    console.log(globalModel)
                    wssGlobal.broadcast(JSON.stringify(globalModel));
                    sleep(500);
                    globalModel = ''
                }

            }, 500);

            ws.on('close', function() {
                console.log('Client has disconnected');
            });
        });

        let conAndWallet = getConnectionProfileAndWalletPath(orgName)
        let ccp = conAndWallet.ccp
        let walletPath = conAndWallet.walletPath

        const walletObj = await buildWallet(Wallets, walletPath);

        const gateway = new Gateway();
        const userIdentity = await walletObj.get(username);

        await gateway.connect(ccp, {
            walletObj,
            identity: userIdentity,
            discovery: {enabled: true, asLocalhost: true}
        });

        const network = await gateway.getNetwork(channel);
        const contract = network.getContract(chaincode);

        const network2 = await gateway.getNetwork('federetedlearning');
        const contract2 = network2.getContract('eventsCC');


        // On reset retrieve last event from BCH and counter value
        let elem = await contract2.submitTransaction('getLastEvent', 1)
        console.log(elem.toString())
        let elemJson = JSON.parse(elem.toString())
        counter = 0
        if (elemJson.length !== 0){
            counter = parseInt(elemJson[0]['counter'])
            lastEvent = [elemJson[0]['roundNo'],elemJson[0]['experimentID'],elemJson[0]['modelsLen'],elemJson[0]['ID']].toString()
        }

        var queueSetup = setInterval(async () => {
            if (queue.length !== 0 && !locked){
                while (queue.length  !== 0) {
                    locked = true
                    let assetVa = queue.shift();
                    try {
                        let getEventLast = await contract2.submitTransaction('getLastEvent', 1)
                        let eventString = getEventLast.toString()
                        console.log(eventString)
                        let eventJson = JSON.parse(eventString)
                        let getEventLastCheck = eventJson.length
                        let compareString = [eventJson[0]['roundNo'],eventJson[0]['experimentID'], eventJson[0]['modelsLen'],eventJson[0]['ID']].toString()
                        if (lastEvent !== compareString) {
                            lastEvent = compareString
                            console.log(lastEvent)
                            counter = 0
                            assets = []
                        }
                        if (getEventLastCheck !== 0 && eventJson[0]['roundNo'] === assetVa["roundNo"] &&
                            eventJson[0]['experimentID'] === assetVa['experimentID']) {
                            counter = counter + 1
                            assets.push(assetVa)
                            console.log(counter)
                            let checkNum = parseInt(eventJson[0]['modelsLen'])
                            if (counter === checkNum) {
                                console.log('Test is done, all good!')
                                data_out = assets
                                console.log(data_out)
                                console.log('Event updated')
                                assets = [];
                            }
                            console.log('Invoke local model event done')
                        }
                    } catch(error){
                        assets.unshift(assetVa)
                    }
                }
                await contract2.submitTransaction('updateCounter',lastEvent.split(',')[3],
                    counter.toString())
                locked = false
            }
        }, 1000)

        try {
            try {
                // first create a listener to be notified of chaincode code events coming from the chaincode ID "events"
                listener = async (event) => {

                    /* The payload of the chaincode event is the value place there by the
                     chaincode. Notice it is a byte data and the application will have
                     to know how to deserialize.
                     In this case we know that the chaincode will always place the asset
                     being worked with as the payload for all events produced. */
                    const asset = JSON.parse(event.payload.toString());
                    console.log(`This is JSON asset var ${JSON.stringify(asset)}${RESET}`)
                    const eventTransaction = event.getTransactionEvent();
                    console.log(`*** transaction: ${eventTransaction.transactionId} status:${eventTransaction.status}`);
                    // showTransactionData(eventTransaction.transactionData);
                    console.log(`${GREEN}<-- Contract Event Received: ${event.eventName} - ${JSON.stringify(asset)}${RESET}`);
                    if (event.eventName === 'local-model-invoke') {
                        queue.push(asset)

                    }
                    if (event.eventName === 'global-model-invoke'){
                        let roundNumber = asset['roundNo']
                        let ext = "json"
                        let cid = asset['CID']
                        var name = 'test.' + ext
                        console.log(cid, ext, name)

                        const client = create()
                        const result = await client.cat(cid)
                        for await (const item of result) {
                            console.log(item)
                            fs.appendFileSync(name, item)
                        }

                        let fileBuffer = fs.readFileSync(name)
                        console.log(fileBuffer)
                        let str = fileBuffer.toString('base64')
                        fs.unlinkSync(name)
                        console.log('Temp file removed')
                        let dataOut = {
                            roundNumber: roundNumber,
                            CID: cid,
                            model: str
                        }
                        globalModel = dataOut
                        await sleep(500);
                        console.log('Invoke global model done ')
                    }
                };
                // now start the client side event service and register the listener
                console.log(`${GREEN}--> Start contract event stream to peer in organization ${orgName}${RESET}`);
                await contract.addContractListener(listener);
            } catch (eventError) {
                console.log(`${RED}<-- Failed: Setup contract events - ${eventError}${RESET}`);
            }

        } catch (runError) {
            console.error(`Error in transaction: ${runError}`);
            if (runError.stack) {
                console.error(runError.stack);
            }
        }
    } catch (error) {
        console.error(`Error in setup: ${error}`);
        if (error.stack) {
            console.error(error.stack);
        }
        process.exit(1);
    }

    await sleep(5000);
}
main();
