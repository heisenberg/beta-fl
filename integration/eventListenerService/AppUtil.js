/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const fs = require('fs');
const path = require('path');

//CONSTANTS
//exports.walletPathSupplyChainActors = path.join(__dirname, 'wallets', 'wallet-SupplyChainActors');
    exports.walletPathSupplyChainActors= path.resolve(__dirname, '..' ,'..', '..' , '..' , '..' , '..' , '..','..','home' , 'sasha_pesic', 'ft-chain-blockchain','hfagent', 'wallets', 'wallet-SupplyChainActors');
console.log('walletPathSupplyChainActors: ' + exports.walletPathSupplyChainActors);
exports.walletPathWholesalers = path.join(__dirname, 'wallets', 'wallet-Wholesalers');
exports.walletPathRetailers = path.join(__dirname,'wallets', 'wallet-Retailers');
exports.walletPathAiServiceProviders = path.join(__dirname,'wallets', 'wallet-AiServiceProviders');
exports.walletPathPlatformOperator = path.join(__dirname,'wallets', 'wallet-PlatformOperator');
exports.walletPathExternalParties = path.join(__dirname,'wallets', 'wallet-ExternalParties');
exports.walletPathConsumers = path.join(__dirname,'wallets', 'wallet-Consumers');

exports.buildCCPSupplyChainActors = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..' ,'..', '..' , '..' , '..' , '..' , '..','..','home' , 'sasha_pesic', 'ft-chain-blockchain', 'network', 'organizations', 'peerOrganizations', 'sca.ftchain.com', 'connection-sca.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPAiServiceProviders = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..',  'network', 'organizations', 'peerOrganizations', 'aiproviders.ftchain.com', 'connection-aiproviders.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPPlatformOperator = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..',  'network', 'organizations', 'peerOrganizations', 'po.ftchain.com', 'connection-po.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPRetailers = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..',  'network', 'organizations', 'peerOrganizations', 'retailers.ftchain.com', 'connection-retailers.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPWholesalers = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..',  'network', 'organizations', 'peerOrganizations', 'wholesalers.ftchain.com', 'connection-wholesalers.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPConsumers = () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..',  'network', 'organizations', 'peerOrganizations', 'consumers.ftchain.com', 'connection-consumers.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildCCPExternalParties= () => {
    // load the common connection configuration file
    const ccpPath = path.resolve(__dirname, '..', '..', 'network', 'organizations', 'peerOrganizations', 'expats.ftchain.com', 'connection-expats.json');
    const fileExists = fs.existsSync(ccpPath);
    if (!fileExists) {
        throw new Error(`no such file or directory: ${ccpPath}`);
    }
    const contents = fs.readFileSync(ccpPath, 'utf8');

    // build a JSON object from the file contents
    const ccp = JSON.parse(contents);

    console.log(`Loaded the network configuration located at ${ccpPath}`);
    return ccp;
};

exports.buildWallet = async (Wallets, walletPath) => {
    // Create a new  wallet : Note that wallet is for managing identities.
    let wallet;
    if (walletPath) {
        wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Built a file system wallet at ${walletPath}`);
    } else {
        wallet = await Wallets.newInMemoryWallet();
        console.log('Built an in memory wallet');
    }
    console.log("*** Returning wallet ***")
    console.log(JSON.stringify(wallet))

    return wallet;
};

exports.getConnectionProfileAndWalletPath = (orgName) => {
    let ccp;
    let walletPath;
    switch (orgName) {
        case 'SupplyChainActors':
            console.log(`Creating a CCP of SupplyChainActors`);
            ccp = this.buildCCPSupplyChainActors();
            walletPath = this.walletPathSupplyChainActors
            break;
        case 'Wholesalers':
            console.log(`Creating a CCP of Wholesalers`);
            ccp = this.buildCCPWholesalers();
            walletPath = this.walletPathWholesalers
            break;
        case 'Retailers':
            console.log(`Creating a CCP of Retailers`);
            ccp = this.buildCCPRetailers();
            walletPath = this.walletPathRetailers
            break;
        case 'AiServiceProviders':
            console.log(`Creating a CCP of AiServiceProviders`);
            ccp = this.buildCCPAiServiceProviders();
            walletPath = this.walletPathAiServiceProviders
            break;
        case 'PlatformOperator':
            console.log(`Creating a CCP of PlatformOperator`);
            ccp = this.buildCCPPlatformOperator();
            walletPath = this.walletPathPlatformOperator
            break;
        case 'Consumers':
            console.log(`Creating a CCP of Consumers`);
            ccp = this.buildCCPConsumers();
            walletPath = this.walletPathConsumers
            break;
        case 'ExternalParties':
            console.log(`Creating a CCP of ExternalParties`);
            ccp = this.buildCCPExternalParties();
            walletPath = this.walletPathExternalParties
            break;
        default:
            ccp = false
            walletPath = false
            break;
    }
    return {ccp:ccp, walletPath:walletPath}

}
exports.prettyJSONString = (inputString) => {
    if (inputString) {
        return JSON.stringify(JSON.parse(inputString), null, 2);
    }
    else {
        return inputString;
    }
}