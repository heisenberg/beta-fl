var request = require('request');

var orgName = 'SupplyChainActors'
var username ='test'
var attrs = [{"name":"location","value":"DE","ecert":true}]
var password = "RandomPass123"

var channel ='supplychains'
var chaincode = 'dataEntriesMg'
var fcn = "getAllEntries"
var args = []

var options = {
    'method': 'POST',
    'url': 'http://ip:4000/users/register',
    'headers': {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({"username":username,"orgName":orgName,"attrs":attrs,"password":password})

};
request(options, function (error, response) {
    if (error) throw new Error(error);
    console.log("Registration & Enrollment response");
    console.log(response.body);
    var token = JSON.parse(response.body).token
    var options = {
        'method': 'POST',
        'url': `http://ip:4000/channels/${channel}/chaincodes/${chaincode}/query`,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({"fcn":fcn,"args":args})

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log("Query response");
        console.log(response.body);
    });

});
