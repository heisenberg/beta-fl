# Food Supply Chain


This repository consists of the blockchain architecture configuration files, smart contracts and integration artifacts for the FT-CHAIN project. Core technologies are Hyperledger Fabric v2.2.2 and NodeJS >v10.12.

#### FT-CHAIN 

FT-CHAIN is AI based supply chain management platform that incorporates privacy through blockchain and federated learning.

Instead of centrally collecting expensive training data for a specific use case and then deploying the sensors across the supply chain, we propose a novel trusted federated learning approach to learn prediction models for these kinds of sensors in a decentralized fashion, wherein the data collection task is distributed along the whole supply chain.

Solution diagram:
![Figure 1 - Solution Diagram](./docs/imgs/solution-diagram.png)

#### PhasmaFOOD

We are using PhasmaFood datasets for training the models for FT-CHAIN.
The main objective of the PhasmaFOOD is to design and implement a multi-target food sensitive mini-portable system for on-the-spot food quality sensing and shelf-life prediction. The PhasmaFOOD miniaturized smart integrated system will be able to detect food hazards, spoilage -including early signs of spoilage-, and food fraud through heterogeneous micro-scale photonics. The connected device will be integrated with a parameterized, knowledge-based, software architecture for on-the-spot food quality sensing and shelf-life prediction.

PhasmaFood Architecture shown on Figure 2:

![Figure 2 - PhasmaFood Diagram](./docs/imgs/phasma_food.png)


### Blockchain network configuration

Complete food supply chain is operated through the secured platform, created with Hyperledger Fabric open source framework.
HLF (Hyperledger Fabric) is open source framework for creating private blockchain networks.

##### Key concepts of HLF:

- Channels (Secured channel between defined organizations that can communicate privately inside network)
- Peers (Nodes that are participants of the network)
- Organizations (Main part of the HLF network, participant of the channels, every organization has specific number of peers)
- Orderer (Organization used for creating block inside network and manipulating with set of rules on channels)
- MSP (Provider of certificates and node roles in the system)


Specified channels and organizations inside FT-CHAIN network shown on Figure 3:
![Figure 2 - PhasmaFood Diagram](./docs/imgs/channels.png)


### Folder structure

The repository has the following structure:

- contracts: contains smart contract folders. 
- hfagent: contains the REST API service that is used for integration with the blockchain network
- integration: NodeJS code samples how to integrate with the hfagent (call REST APIs)
- network: configuration, docker, crypto-config, fabric-ca files, helper bash scripts for easy network management. 

### How to use

To pull HF binaries: 
```bash
curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.2.2 1.4.9
```

To bring up the network docker containers and create all necessary channels:
```bash
./network.sh up createChannel
```

To bring up the Hyperledger Explorer containers navigate to network/explorer:
```bash
docker-compose -f docker-compose.yaml up -d
```

To deploy a smart contract: 
```bash
./network.sh deployCC -ccn "contract name" -ccv 1.0 -ccp "contract path" -ccl javascript -c "channel name" -ccs 1
```
where:

- ccn: smart contract name
- ccv: smart contract version
- ccp: path to smart contract folder
- ccl: language
- c: name of the channel where you are deploying to
- ccs: sequence number

To deploy an upgraded version of the same smart contract simply increase its version and sequence. 

Hyperledger explorer is accessible on this address: http://ip:8080/. Login details are: exploreradmin / exploreradminpw

To stop the network: 
```bash
./network.sh down
```

### Additional helpful commands

To view real-time docker logs: 
```bash
docker ps -q | xargs -L 1 -P `docker ps | wc -l` docker logs --since 30s -f
```

Remove all docker containers with a naming pattern (dev in the case below):
```bash
docker rm -f $(docker ps -a | awk '{ print $1,$2 }' | grep dev | awk '{print $1}')
```
