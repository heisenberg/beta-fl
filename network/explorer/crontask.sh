
#!/bin/sh
echo " " > /root/crontask.txt
echo "Start time is: " $(date +%T) >> /root/crontask.txt
		
docker stop explorer.ftchain.com explorerdb.ftchain.com  >> /root/crontask.txt
docker rm explorer.ftchain.com explorerdb.ftchain.com  >> /root/crontask.txt
docker volume prune -f >> /root/crontask.txt
docker image prune -a -f >> /root/crontask.txt 
docker container prune -f >> /root/crontask.txt 
docker system prune -f >> /root/crontask.txt

cd /home/sasha_pesic/ft-chain-blockchain/network/explorer 
/usr/local/bin/docker-compose -f docker-compose.yaml up -d 

echo "End time is: " $(date +%T) >> /root/crontask.txt'
