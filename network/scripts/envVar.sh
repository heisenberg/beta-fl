#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# This is a collection of bash functions used by different scripts

# imports
. scripts/utils.sh

export CORE_PEER_TLS_ENABLED=true
export ORDERER_CA=${PWD}/organizations/ordererOrganizations/ftchain.com/orderers/orderer.ftchain.com/msp/tlscacerts/tlsca.ftchain.com-cert.pem

export PEER0_SupplyChainActors_CA=${PWD}/organizations/peerOrganizations/sca.ftchain.com/peers/peer0.sca.ftchain.com/tls/ca.crt
export PEER0_Wholesalers_CA=${PWD}/organizations/peerOrganizations/wholesalers.ftchain.com/peers/peer0.wholesalers.ftchain.com/tls/ca.crt
export PEER0_Retailers_CA=${PWD}/organizations/peerOrganizations/retailers.ftchain.com/peers/peer0.retailers.ftchain.com/tls/ca.crt
export PEER0_AiServiceProviders_CA=${PWD}/organizations/peerOrganizations/aiproviders.ftchain.com/peers/peer0.aiproviders.ftchain.com/tls/ca.crt
export PEER0_PlatformOperator_CA=${PWD}/organizations/peerOrganizations/po.ftchain.com/peers/peer0.po.ftchain.com/tls/ca.crt
export PEER0_ExternalParties_CA=${PWD}/organizations/peerOrganizations/expats.ftchain.com/peers/peer0.expats.ftchain.com/tls/ca.crt
export PEER0_Consumers_CA=${PWD}/organizations/peerOrganizations/consumers.ftchain.com/peers/peer0.consumers.ftchain.com/tls/ca.crt

# Set environment variables for the peer org
setGlobals() {
  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$1
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  infoln "Using organization ${USING_ORG}"
  if [ $USING_ORG = "SupplyChainActors" ]; then
    export CORE_PEER_LOCALMSPID="SupplyChainActorsMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_SupplyChainActors_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/sca.ftchain.com/users/Admin@sca.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:7051
  elif [ $USING_ORG = "Wholesalers" ]; then
    export CORE_PEER_LOCALMSPID="WholesalersMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Wholesalers_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/wholesalers.ftchain.com/users/Admin@wholesalers.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:8051
  elif [ $USING_ORG = "Retailers" ]; then
    export CORE_PEER_LOCALMSPID="RetailersMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Retailers_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/retailers.ftchain.com/users/Admin@retailers.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:9051
  elif [ $USING_ORG = "AiServiceProviders" ]; then
    export CORE_PEER_LOCALMSPID="AiServiceProvidersMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_AiServiceProviders_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/aiproviders.ftchain.com/users/Admin@aiproviders.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:10051
  elif [ $USING_ORG = "PlatformOperator" ]; then
    export CORE_PEER_LOCALMSPID="PlatformOperatorMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_PlatformOperator_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/po.ftchain.com/users/Admin@po.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:11051
  elif [ $USING_ORG = "ExternalParties" ]; then
    export CORE_PEER_LOCALMSPID="ExternalPartiesMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ExternalParties_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/expats.ftchain.com/users/Admin@expats.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:12051
  elif [ $USING_ORG = "Consumers" ]; then
    export CORE_PEER_LOCALMSPID="ConsumersMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Consumers_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/consumers.ftchain.com/users/Admin@consumers.ftchain.com/msp
    export CORE_PEER_ADDRESS=localhost:13051
  else
    errorln "ORG Unknown"
  fi

  if [ "$VERBOSE" = "true" ]; then
    env | grep CORE
  fi
}

# Set environment variables for use in the CLI container 
setGlobalsCLI() {
  setGlobals $1

  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$1
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  if [ $USING_ORG = "SupplyChainActors" ]; then
    export CORE_PEER_ADDRESS=peer0.sca.ftchain.com:7051
  elif [ $USING_ORG = "Wholesalers" ]; then
    export CORE_PEER_ADDRESS=peer0.wholesalers.ftchain.com:8051
  elif [ $USING_ORG = "Retailers" ]; then
    export CORE_PEER_ADDRESS=peer0.retailers.ftchain.com:9051
  elif [ $USING_ORG = "AiServiceProviders" ]; then
    export CORE_PEER_ADDRESS=peer0.aiproviders.ftchain.com:10051
  elif [ $USING_ORG = "PlatformOperator" ]; then
    export CORE_PEER_ADDRESS=peer0.po.ftchain.com:11051
  elif [ $USING_ORG = "ExternalParties" ]; then
    export CORE_PEER_ADDRESS=peer0.expats.ftchain.com:12051
  elif [ $USING_ORG = "Consumers" ]; then
    export CORE_PEER_ADDRESS=peer0.consumers.ftchain.com:13051
  else
    errorln "ORG Unknown"
  fi
}

# parsePeerConnectionParameters $@
# Helper function that sets the peer connection parameters for a chaincode
# operation
parsePeerConnectionParameters() {
  PEER_CONN_PARMS=""
  PEERS=""
  while [ "$#" -gt 0 ]; do
    setGlobals $1
    if [ $1 = "SupplyChainActors" ]; then
        PEER=peer0.sca
    elif [ $1 = "Wholesalers" ]; then
        PEER=peer0.wholesalers
    elif [ $1 = "Retailers" ]; then
        PEER=peer0.retailers
    elif [ $1 = "AiServiceProviders" ]; then
        PEER=peer0.aiproviders
    elif [ $1 = "PlatformOperator" ]; then
        PEER=peer0.po
    elif [ $1 = "ExternalParties" ]; then
        PEER=peer0.expats
    elif [ $1 = "Consumers" ]; then
        PEER=peer0.consumers
    else
        errorln "ORG Unknown"
    fi
#    PEER="peer0.org$1"
    ## Set peer addresses
    PEERS="$PEERS $PEER"
    PEER_CONN_PARMS="$PEER_CONN_PARMS --peerAddresses $CORE_PEER_ADDRESS"
    ## Set path to TLS certificate sample PEER0_SupplyChainActors_CA
    TLSINFO=$(eval echo "--tlsRootCertFiles \$PEER0_$1_CA")
    PEER_CONN_PARMS="$PEER_CONN_PARMS $TLSINFO"
    # shift by one to get to the next organization
    shift
  done
  # remove leading space for output
  PEERS="$(echo -e "$PEERS" | sed -e 's/^[[:space:]]*//')"
}

verifyResult() {
  if [ $1 -ne 0 ]; then
    fatalln "$2"
  fi
}
