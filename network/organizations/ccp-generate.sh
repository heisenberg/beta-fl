#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s#\${PEERORG}#$6#" \
        organizations/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s#\${PEERORG}#$6#" \
        organizations/ccp-template.yaml | sed -e $'s/\\\\n/\\\n          /g'
}

ORG=SupplyChainActors
PEERORG=sca
P0PORT=7051
CAPORT=7054
PEERPEM=organizations/peerOrganizations/sca.ftchain.com/tlsca/tlsca.sca.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/sca.ftchain.com/ca/ca.sca.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/sca.ftchain.com/connection-sca.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/sca.ftchain.com/connection-sca.yaml


ORG=Wholesalers
PEERORG=wholesalers
P0PORT=8051
CAPORT=8054
PEERPEM=organizations/peerOrganizations/wholesalers.ftchain.com/tlsca/tlsca.wholesalers.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/wholesalers.ftchain.com/ca/ca.wholesalers.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/wholesalers.ftchain.com/connection-wholesalers.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/wholesalers.ftchain.com/connection-wholesalers.yaml

ORG=Retailers
PEERORG=retailers
P0PORT=9051
CAPORT=9054
PEERPEM=organizations/peerOrganizations/retailers.ftchain.com/tlsca/tlsca.retailers.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/retailers.ftchain.com/ca/ca.retailers.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/retailers.ftchain.com/connection-retailers.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/retailers.ftchain.com/connection-retailers.yaml

ORG=AiServiceProviders
PEERORG=aiproviders
P0PORT=10051
CAPORT=10054
PEERPEM=organizations/peerOrganizations/aiproviders.ftchain.com/tlsca/tlsca.aiproviders.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/aiproviders.ftchain.com/ca/ca.aiproviders.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/aiproviders.ftchain.com/connection-aiproviders.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/aiproviders.ftchain.com/connection-aiproviders.yaml


ORG=PlatformOperator
PEERORG=po
P0PORT=11051
CAPORT=11054
PEERPEM=organizations/peerOrganizations/po.ftchain.com/tlsca/tlsca.po.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/po.ftchain.com/ca/ca.po.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/po.ftchain.com/connection-po.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/po.ftchain.com/connection-po.yaml


ORG=ExternalParties
P0PORT=12051
CAPORT=12054
PEERPEM=organizations/peerOrganizations/expats.ftchain.com/tlsca/tlsca.expats.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/expats.ftchain.com/ca/ca.expats.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/expats.ftchain.com/connection-expats.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/expats.ftchain.com/connection-expats.yaml

ORG=Consumers
PEERORG=consumers
P0PORT=13051
CAPORT=13054
PEERPEM=organizations/peerOrganizations/consumers.ftchain.com/tlsca/tlsca.consumers.ftchain.com-cert.pem
CAPEM=organizations/peerOrganizations/consumers.ftchain.com/ca/ca.consumers.ftchain.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/consumers.ftchain.com/connection-consumers.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERORG)" > organizations/peerOrganizations/consumers.ftchain.com/connection-consumers.yaml