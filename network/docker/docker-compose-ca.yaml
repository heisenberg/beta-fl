# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: '2'

networks:
  test:
    name: test-network

services:
  ca.sca.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=SupplyChainActorsCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=7054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.sca.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "7054:7054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.sca.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/sca.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/SupplyChainActors/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.sca.ftchain.com
    networks:
      - test

  ca.wholesalers.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=WholesalersCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=8054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.wholesalers.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "8054:8054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.wholesalers.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/wholesalers.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/Wholesalers/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.wholesalers.ftchain.com
    networks:
      - test

  ca.retailers.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=RetailersCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=9054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.retailers.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "9054:9054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.retailers.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/retailers.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/Retailers/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.retailers.ftchain.com
    networks:
      - test

  ca.aiproviders.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=AiServiceProvidersCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=10054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.aiproviders.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "10054:10054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.aiproviders.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/aiproviders.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/AiServiceProviders/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.aiproviders.ftchain.com
    networks:
      - test

  ca.po.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=PlatformOperatorCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=11054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.po.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "11054:11054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.po.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/po.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/PlatformOperator/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.po.ftchain.com
    networks:
      - test

  ca.expats.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ExternalPartiesCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=12054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.expats.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "12054:12054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.expats.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/expats.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/ExternalParties/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.expats.ftchain.com
    networks:
      - test

  ca.consumers.ftchain.com:
    image: hyperledger/fabric-ca:latest
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ConsumersCA
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_PORT=13054
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server/ca.consumers.ftchain.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server/priv_sk
    ports:
      - "13054:13054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server/ca.consumers.ftchain.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server/priv_sk -b admin:adminpw -d'
    volumes:
      - ../organizations/peerOrganizations/consumers.ftchain.com/ca/:/etc/hyperledger/fabric-ca-server
      - ../organizations/fabric-ca/Consumers/fabric-ca-server-config.yaml:/etc/hyperledger/fabric-ca-server/fabric-ca-server-config.yaml
    container_name: ca.consumers.ftchain.com
    networks:
      - test
