# To run FL training experiment
## For running Synchronous experiments 
Run python scripts by providing experiment id in argument
e.g. without blockchain
```bash
python sync_FL_wo_BC.py -e name_of_experiment
```
or 

with blockchain
```bash
python sync_FL_wt_BC.py -e name_of_experiment
```
### For parallelizing work flow Ray is used
Ray core needs to be started in the background
```bash
ray start --head
```
## For running Asynchronous experiments 

1.  ```server_script.py``` needs to be started with provided experiment_id
```bash
python server_script.py -e name_of_experiment
```
2. in seprate terminal ```client_script.py``` is started with same experiment_id of server and run_id provided by server_script.
```bash
python client_script.py -e name_of_experiment -r run_id_of_server
```
To kill ray core, run the following command
```bash
ray stop
```
### Getting logs from MLFlow 
- if SQLite is database

```bash
 mlflow ui --backend-store-uri=sqlite:///mlruns.db -h 0.0.0.0 
 ```

Note: python depedencies are provided in ```requirements.txt``` that can be installed in virtual environment.