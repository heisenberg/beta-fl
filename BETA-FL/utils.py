from time import pthread_getcpuclockid
from pyparsing import python_style_comment
import torch
import numpy as np

DEVICE = torch.device("cpu")
from collections import OrderedDict
from sklearn.metrics import confusion_matrix
import json
from json import JSONEncoder
import numpy
from functools import reduce
from typing import List, Optional, Tuple


def train(net, trainloader, epochs):
    """Train the network on the training set."""
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
    net.train()
    for epoch in range(epochs):
        for images, labels in trainloader:
            images, labels = images.to(DEVICE), labels.to(DEVICE)
            optimizer.zero_grad()
            loss = criterion(net(images), labels)
            loss.backward()
            optimizer.step()
        print(f"train loss at epoch {epoch} : {loss}")
    return loss, net


def test(net, testloader):
    """Validate the network on the entire test set."""
    criterion = torch.nn.BCEWithLogitsLoss()
    correct, total, loss = 0, 0, 0.0
    labels_, predicted_ = [], []
    net.eval()
    with torch.no_grad():
        for data in testloader:
            images, labels = data[0].to(DEVICE), data[1].to(DEVICE)
            outputs = net(images)
            # loss += criterion(outputs, labels.float()).item()
            predicted = torch.argmax(outputs.data, 1)

            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            predicted_.append(predicted.numpy())
            labels_.append(labels.numpy())

    accuracy = correct / total
    print(confusion_matrix(np.concatenate(labels_), np.concatenate(predicted_)))
    return loss, accuracy

    # Unpack the CIFAR-10 dataset partition
    # trainloader, testloader = dataset


class FLClient:
    def __init__(self, model, trainloader, testloader):
        self.net = model
        self.trainloader = trainloader
        self.testloader = testloader

    def get_parameters(self):
        return [val.cpu().numpy() for _, val in self.net.state_dict().items()]

    def set_parameters(self, parameters):
        params_dict = zip(self.net.state_dict().keys(), parameters)
        state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
        self.net.load_state_dict(state_dict, strict=True)
        print("set_parameters")

    def fit(self, parameters, config):
        self.set_parameters(parameters)
        train(self.net, self.trainloader, epochs=config["epochs"])
        print("finished training")
        return self.get_parameters(), len(self.trainloader.dataset), {}

    def evaluate(self, parameters, config):
        self.set_parameters(parameters)
        loss, accuracy = test(self.net, self.testloader)

        return float(loss), len(self.testloader.dataset), {"accuracy": float(accuracy)}

    def state_dict_to_json(self, parameters):
        self.set_parameters(parameters)
        torch.save(self.net.state_dict(), "sample.pth")
        return "written to json"


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


def aggregate(results):
    """Compute weighted average."""
    # Calculate the total number of examples used during training
    num_examples_total = sum([num_examples for _, num_examples in results])

    # Create a list of weights, each multiplied by the related number of examples
    weighted_weights = [
        [layer * num_examples for layer in weights] for weights, num_examples in results
    ]

    # Compute average weights of each layer
    weights_prime = [
        reduce(np.add, layer_updates) / num_examples_total
        for layer_updates in zip(*weighted_weights)
    ]
    return weights_prime


if __name__ == "__main__":
    numpyArrayOne = numpy.array([[11, 22, 33], [44, 55, 66], [77, 88, 99]])

    # Serialization
    numpyData = {"array": numpyArrayOne}
    encodedNumpyData = json.dumps(
        numpyData, cls=NumpyArrayEncoder
    )  # use dump() to write array into file
    print("Printing JSON serialized NumPy array")
    print(encodedNumpyData)

    # Deserialization
    print("Decode JSON serialized NumPy array")
    decodedArrays = json.loads(encodedNumpyData)

    finalNumpyArray = numpy.asarray(decodedArrays["array"])
    print("NumPy Array")
    print(finalNumpyArray)
