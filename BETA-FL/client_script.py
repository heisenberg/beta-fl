import torch
from MPNet import MPNet
import numpy as np
import json
import os
from chain_code import *
from utils import FLClient, aggregate
import websockets
import argparse
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
import websocket
import rel
from time import sleep
import time

DEVICE = torch.device("cpu")

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--exp_id", type=str, help="experiment_id", required=True)
parser.add_argument("-r", "--run_id", type=str, help="mlflow run_id", required=True)
parser.add_argument("-n", "--roundNum", type=int, help="roundNum", default=2)
args_partition = parser.parse_args()
EXP_ID = args_partition.exp_id
ROUNDS = args_partition.roundNum

from mlflow import log_metric, log_param, log_artifacts
import mlflow

mlflow.set_tracking_uri("sqlite:///mlruns.db")
EVENT_COND = 4

from custom_logger import logger_config

if not os.path.exists(f"logs"):
    os.makedirs(f"logs")
from custom_logger import logger_config

my_logger = logger_config(f"logs/{EXP_ID}")


class AsyncClients:
    def __init__(self):
        self.local_loss, self.local_accuracy = [], []
        eventUpdate(str(1), EVENT_COND, EXP_ID, register_member())
        self.train_clients(curr_round=1)

    def train_clients(self, curr_round, client_count=4, initial_params=None, epochs=5):
        print("training clients for round :" + str(curr_round))
        if curr_round == 1:
            initial_params = get_file_from_ipfs(
                self.InitializeGlobalModel(), register_member()
            )
        else:
            print("loaded weights from prev round")

        initial_weights = [
            np.array(val) for _, val in json.loads(initial_params).items()
        ]

        client_loss = []
        client_accu = []
        for i in range(client_count):
            client_id = i
            x_train_0 = torch.Tensor(
                np.load(f"client_data/x_client_{client_id}.npy").reshape(-1, 1, 200)
            )
            y_train_0 = torch.LongTensor(
                np.load(f"client_data/y_client_{client_id}.npy").astype(int)
            )

            train_dataset = TensorDataset(x_train_0, y_train_0)
            train_dataloader = DataLoader(train_dataset, batch_size=32)

            x_test = torch.Tensor(np.load("client_data/x_test.npy").reshape(-1, 1, 200))
            y_test = torch.LongTensor(np.load("client_data/y_test.npy").astype(int))

            test_dataset = TensorDataset(x_test, y_test)
            test_dataloader = DataLoader(test_dataset, batch_size=32)
            # trainloader, testloader = dataset.load(client_count)[client_id]

            fl_client = FLClient(MPNet().to(DEVICE), train_dataloader, test_dataloader)
            fl_client.set_parameters(initial_weights)
            output_params, len_trainloader, _ = fl_client.fit(
                initial_weights,
                {"epochs": epochs},
            )
            loss, len_testloader, metrics = fl_client.evaluate(output_params, {})
            print(f'local test loss:{loss}, accuracy:{metrics["accuracy"]}')
            with mlflow.start_run(run_id=args_partition.run_id, nested=True):
                log_metric(
                    f"client_accuracy_{client_id}", metrics["accuracy"], step=curr_round
                )

            client_loss.append(loss)
            client_accu.append(metrics["accuracy"])
            start_time = time.time()
            output_dict = {k: val.tolist() for k, val in enumerate(output_params)}
            encodedLocalParam = json.dumps(output_dict)
            with open(
                f"encodedLocalParam_temp_{client_id}_r_{curr_round}.json", "w"
            ) as write_file:
                json.dump(encodedLocalParam, write_file)
            hash_ipfs = add_file_to_ipfs(
                f"encodedLocalParam_temp_{client_id}_r_{curr_round}.json",
                register_member(),
            )
            isResponseOkay = False
            maxNoAttemtps = 10
            attemptCounter = 0
            while not isResponseOkay and attemptCounter < maxNoAttemtps:
                response = uploadLocalModelExperiment(
                    str(hash_ipfs),
                    str(curr_round),
                    str(len_trainloader),
                    EXP_ID,
                    register_member(),
                )
                isResponseOkay = response["success"]
                attemptCounter = attemptCounter + 1
                sleep(0.5)

            os.remove(f"encodedLocalParam_temp_{client_id}_r_{curr_round}.json")
            end_time = time.time()
            my_logger.info(
                f"Execution time for client{client_id} at round {curr_round}: {end_time-start_time} seconds"
            )
        self.local_loss.append(client_loss)
        self.local_accuracy.append(client_accu)
        my_logger.info(f"Finished 4th client from round{curr_round} at: {time.time()}")

    def InitializeGlobalModel(self):
        global_weights_json = {
            key: val.cpu().numpy().tolist()
            for key, val in MPNet().to(DEVICE).state_dict().items()
        }
        encodedJsonAggData = json.dumps(global_weights_json)
        with open(f"encodedJsonAggData_r_{0}.json", "w") as write_file:
            json.dump(encodedJsonAggData, write_file)
            hash_ipfs = add_file_to_ipfs(
                f"encodedJsonAggData_r_{0}.json", register_member()
            )
            # uploadGlobalModelExperiment(hash_ipfs, str(0), EXP_ID, register_member())
            os.remove(f"encodedJsonAggData_r_{0}.json")
        return hash_ipfs

    def save_logs_local(self):
        np.savetxt("local_loss.csv", self.local_loss, delimiter=", ", fmt="% s")
        np.savetxt("local_accuracy.csv", self.local_accuracy, delimiter=", ", fmt="% s")


class GlobalListener:
    def __init__(self, max_round):
        self.max_round = max_round
        self.start_time = time.time()

    def on_message(self, ws, message):
        print("Message received from global:")
        for k, v in json.loads(message).items():
            print(f"keys:{k}")
        curr_round = int(json.loads(message)["roundNumber"])
        global_hash = json.loads(message)["CID"]
        res_ret = json.loads(message)["model"]
        byte_array = base64.b64decode(res_ret).decode(encoding="utf-8")
        res_dict = json.loads(byte_array)

        if curr_round < self.max_round:
            self.async_clients.train_clients(curr_round + 1, initial_params=res_dict)
        else:
            print(f"Finished training for {self.max_round} rounds")
            end_time = time.time()
            print(f"Execution time:{end_time-self.start_time} seconds")
            self.async_clients.save_logs_local()
            # mlflow.end_run()
            ws.close()

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws, close_status_code, close_msg):
        print("### global listener closed ###")

    def on_open(self, ws):
        print("Opened connection from global listener")

        self.async_clients = AsyncClients()


if __name__ == "__main__":
    global_listener = GlobalListener(ROUNDS)

    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(
        "ws://34.83.215.154:7080",
        on_open=global_listener.on_open,
        on_message=global_listener.on_message,
        on_error=global_listener.on_error,
        on_close=global_listener.on_close,
    )

ws.run_forever(dispatcher=rel)  # Set dispatcher to automatic reconnection
rel.signal(2, rel.abort)  # Keyboard Interrupt
rel.dispatch()
