import torch
from MPNet import MPNet
import numpy as np
import json
import os
from chain_code import *
from utils import FLClient, aggregate
import argparse
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
import time

DEVICE = torch.device("cpu")

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--exp_id", type=str, help="experiment_id", required=True)
parser.add_argument("-n", "--roundNum", type=int, help="roundNum", default=10)

args_partition = parser.parse_args()
EXP_ID = args_partition.exp_id
ROUNDS = args_partition.roundNum

from mlflow import log_metric, log_param, log_artifacts
import mlflow

mlflow.set_tracking_uri("sqlite:///mlruns.db")
mlflow.start_run(nested=True)
run = mlflow.active_run()
EVENT_COND = 4
print(run.info.run_id)

from custom_logger import logger_config

my_logger = logger_config(f"{EXP_ID}")


class AsyncServer:
    def __init__(self):
        self.global_loss, self.global_accuracy = [], []

    def aggregator(self, round_num, weights_list):
        print("client event fired")
        results = [
            (
                [np.array(val) for _, val in json.loads(weight).items()],
                int(count),
            )
            for weight, count in weights_list
        ]

        #
        aggregatedList = aggregate(results)

        print("done aggregate(results)")
        x_train_0 = torch.Tensor(
            np.load(f"client_data/x_client_{0}.npy").reshape(-1, 1, 200)
        )
        y_train_0 = torch.LongTensor(
            np.load(f"client_data/y_client_{0}.npy").astype(int)
        )

        train_dataset = TensorDataset(x_train_0, y_train_0)
        train_dataloader = DataLoader(train_dataset, batch_size=32)

        x_test = torch.Tensor(np.load("client_data/x_test.npy").reshape(-1, 1, 200))
        y_test = torch.LongTensor(np.load("client_data/y_test.npy").astype(int))

        test_dataset = TensorDataset(x_test, y_test)
        test_dataloader = DataLoader(test_dataset, batch_size=32)
        fl_client_global = FLClient(
            MPNet().to(DEVICE), train_dataloader, test_dataloader
        )

        global_weights = aggregatedList
        print(f"len(global_weights):{len(global_weights)} ")
        loss, len_testloader, metrics = fl_client_global.evaluate(global_weights, {})
        print(f'global test loss:{loss}, global accuracy:{metrics["accuracy"]}')
        self.global_loss.append(loss)
        self.global_accuracy.append(metrics["accuracy"])
        with mlflow.start_run(run_id=run.info.run_id, nested=True):
            log_metric(f"global_accuracy", metrics["accuracy"], step=round_num)
        aggregated_dict = {k: val.tolist() for k, val in enumerate(aggregatedList)}

        encodedNumpyAggData = json.dumps(aggregated_dict)

        return encodedNumpyAggData

    def save_logs_global(self):
        np.savetxt("global_loss_wo_bc.csv", self.global_loss, delimiter=", ", fmt="% s")
        np.savetxt(
            "global_accuracy_wo_bc.csv", self.global_accuracy, delimiter=", ", fmt="% s"
        )


class AsyncClients:
    def __init__(self):
        self.local_loss, self.local_accuracy = [], []
        # eventUpdate(str(1), EVENT_COND, EXP_ID, register_member())

    def train_clients(self, curr_round, client_count=4, initial_params=None, epochs=5):
        print("training clients for round :" + str(curr_round))
        if curr_round == 1:
            initial_params = self.InitializeGlobalModel()
        else:
            print("loaded weights from prev round")

        initial_weights = [
            np.array(val) for _, val in json.loads(initial_params).items()
        ]

        client_loss = []
        client_accu = []
        for i in range(client_count):
            start_time = time.time()
            separate_weights = []
            client_id = i
            x_train_0 = torch.Tensor(
                np.load(f"client_data/x_client_{client_id}.npy").reshape(-1, 1, 200)
            )
            y_train_0 = torch.LongTensor(
                np.load(f"client_data/y_client_{client_id}.npy").astype(int)
            )

            train_dataset = TensorDataset(x_train_0, y_train_0)
            train_dataloader = DataLoader(train_dataset, batch_size=32)

            x_test = torch.Tensor(np.load("client_data/x_test.npy").reshape(-1, 1, 200))
            y_test = torch.LongTensor(np.load("client_data/y_test.npy").astype(int))

            test_dataset = TensorDataset(x_test, y_test)
            test_dataloader = DataLoader(test_dataset, batch_size=32)
            # trainloader, testloader = dataset.load(client_count)[client_id]

            fl_client = FLClient(MPNet().to(DEVICE), train_dataloader, test_dataloader)
            fl_client.set_parameters(initial_weights)
            output_params, len_trainloader, _ = fl_client.fit(
                initial_weights,
                {"epochs": epochs},
            )
            loss, len_testloader, metrics = fl_client.evaluate(output_params, {})
            print(f'local test loss:{loss}, accuracy:{metrics["accuracy"]}')
            with mlflow.start_run(run_id=run.info.run_id, nested=True):
                log_metric(
                    f"client_accuracy_{client_id}", metrics["accuracy"], step=curr_round
                )

            client_loss.append(loss)
            client_accu.append(metrics["accuracy"])
            output_dict = {k: val.tolist() for k, val in enumerate(output_params)}
            encodedLocalParam = json.dumps(output_dict)
            separate_weights.append((encodedLocalParam, len_trainloader))
            end_time = time.time()
            my_logger.info(
                f"Training time for client{client_id} at round {curr_round}: {end_time-start_time} seconds"
            )

        self.local_loss.append(client_loss)
        self.local_accuracy.append(client_accu)
        return separate_weights

    def InitializeGlobalModel(self):
        global_weights_json = {
            key: val.cpu().numpy().tolist()
            for key, val in MPNet().to(DEVICE).state_dict().items()
        }
        encodedJsonAggData = json.dumps(global_weights_json)

        return encodedJsonAggData

    def save_logs_local(self):
        np.savetxt("local_loss_wo_bc.csv", self.local_loss, delimiter=", ", fmt="% s")
        np.savetxt(
            "local_accuracy_wo_bc.csv", self.local_accuracy, delimiter=", ", fmt="% s"
        )


if __name__ == "__main__":
    start_time = time.time()
    async_server = AsyncServer()
    async_clients = AsyncClients()

    start_global_wt = None
    for r in range(1, ROUNDS + 1):
        separate_weights = async_clients.train_clients(
            curr_round=r, initial_params=start_global_wt
        )
        res_dict = async_server.aggregator(r, separate_weights)
        start_global_wt = res_dict
    async_clients.save_logs_local()
    async_server.save_logs_global()
    end_time = time.time()
    print(f"Execution time:{end_time-start_time} seconds")
