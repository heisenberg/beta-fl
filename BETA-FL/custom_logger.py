import logging


def logger_config(filename):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    file_handler = logging.FileHandler(f"{filename}.log")
    formatter = logging.Formatter("%(asctime)s: %(levelname)s: %(message)s ")
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    return logger

