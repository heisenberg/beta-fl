import torch
from MPNet import MPNet
import numpy as np
import json
import os
from chain_code import *
from utils import FLClient, aggregate
import argparse
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
import websocket
import rel
import time


DEVICE = torch.device("cpu")

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--exp_id", type=str, help="experiment_id", required=True)
parser.add_argument("-n", "--roundNum", type=int, help="roundNum", default=2)
args_partition = parser.parse_args()
EXP_ID = args_partition.exp_id
ROUNDS = args_partition.roundNum

from mlflow import log_metric, log_param, log_artifacts
import mlflow

mlflow.set_tracking_uri("sqlite:///mlruns.db")
mlflow.start_run(nested=True)
run = mlflow.active_run()
EVENT_COND = 4
print(run.info.run_id)
import os

if not os.path.exists(f"logs"):
    os.makedirs(f"logs")
from custom_logger import logger_config

my_logger = logger_config(f"logs/{EXP_ID}")


class AsyncServer:
    def __init__(self):
        self.global_loss, self.global_accuracy = [], []

    def aggregator(self, round_num):
        print("client event fired")
        my_logger.info(f"Before agg from round{round_num} at: {time.time()}")
        results = [
            (
                [
                    np.array(val)
                    for _, val in json.loads(
                        get_file_from_ipfs(entry["Record"]["CID"], register_member())
                    ).items()
                ],
                int(entry["Record"]["numberOfExamples"]),
            )
            for entry in getLocalModelsFilter(EXP_ID, str(round_num), register_member())
        ]
        if round_num > 1:
            print(
                f"models in curr round : {getLocalModelsFilterLength(EXP_ID, str(round_num), register_member())}"
            )
            print(
                f"models in prev round : {getLocalModelsFilterLength(EXP_ID, str(round_num-1), register_member())}"
            )
        #
        aggregatedList = aggregate(results)
        len_aggregated_models = len(results)

        print("done aggregate(results)")
        x_train_0 = torch.Tensor(
            np.load(f"client_data/x_client_{0}.npy").reshape(-1, 1, 200)
        )
        y_train_0 = torch.LongTensor(
            np.load(f"client_data/y_client_{0}.npy").astype(int)
        )

        train_dataset = TensorDataset(x_train_0, y_train_0)
        train_dataloader = DataLoader(train_dataset, batch_size=32)

        x_test = torch.Tensor(np.load("client_data/x_test.npy").reshape(-1, 1, 200))
        y_test = torch.LongTensor(np.load("client_data/y_test.npy").astype(int))

        test_dataset = TensorDataset(x_test, y_test)
        test_dataloader = DataLoader(test_dataset, batch_size=32)
        fl_client_global = FLClient(
            MPNet().to(DEVICE), train_dataloader, test_dataloader
        )

        global_weights = aggregatedList
        print(f"len(global_weights):{len(global_weights)} ")
        loss, len_testloader, metrics = fl_client_global.evaluate(global_weights, {})
        print(f'global test loss:{loss}, global accuracy:{metrics["accuracy"]}')
        self.global_loss.append(loss)
        self.global_accuracy.append(metrics["accuracy"])
        with mlflow.start_run(run_id=run.info.run_id, nested=True):
            log_metric(f"global_accuracy", metrics["accuracy"], step=round_num)
            log_metric(f"count_models_agg", len_aggregated_models, step=round_num)
        aggregated_dict = {k: val.tolist() for k, val in enumerate(aggregatedList)}

        encodedNumpyAggData = json.dumps(aggregated_dict)
        with open(f"encodedNumpyAggData_r_{round_num}.json", "w") as write_file:
            json.dump(encodedNumpyAggData, write_file)
            print("written aggregated files on disk")
        hash_ipfs = add_file_to_ipfs(
            f"encodedNumpyAggData_r_{round_num}.json", register_member()
        )
        print("written agg on ipfs")
        uploadGlobalModelExperiment(hash_ipfs, round_num, EXP_ID, register_member())
        print("upload global hash_ipfs")
        os.remove(f"encodedNumpyAggData_r_{round_num}.json")
        eventUpdate(str(round_num + 1), EVENT_COND, EXP_ID, register_member())
        if round_num + 1 > ROUNDS:
            exit()

    def save_logs_global(self):
        np.savetxt("global_loss.csv", self.global_loss, delimiter=", ", fmt="% s")
        np.savetxt(
            "global_accuracy.csv", self.global_accuracy, delimiter=", ", fmt="% s"
        )


class LocalListener:
    def __init__(self, max_round):
        self.async_server = AsyncServer()
        self.max_round = max_round

    def on_message(self, ws, message):
        print("Message received from client:")
        # print(message)

        curr_round = int(json.loads(message)[-1]["roundNo"])
        global_hash = json.loads(message)[-1]["CID"]
        self.async_server.aggregator(curr_round)
        self.async_server.save_logs_global()
        if curr_round == self.max_round:
            # mlflow.end_run()
            ws.close()

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws, close_status_code, close_msg):
        print("### local listener closed ###")

    def on_open(self, ws):
        print("Opened connection from local listener")


if __name__ == "__main__":
    local_listener = LocalListener(ROUNDS)
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(
        "ws://34.83.215.154:7070",
        on_open=local_listener.on_open,
        on_message=local_listener.on_message,
        on_error=local_listener.on_error,
        on_close=local_listener.on_close,
    )
ws.run_forever(dispatcher=rel)  # Set dispatcher to automatic reconnection
rel.signal(2, rel.abort)  # Keyboard Interrupt
rel.dispatch()
