'use strict'

const StateList = require('./ledger-api/statelist.js')

const checkpointEntryRecord = require('./CheckpointEntryRecord.js')

class CheckpointEntryRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.checkpointentryrecordlist')
        this.use(checkpointEntryRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = CheckpointEntryRecordList