'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class CheckpointEntryRecord extends State {

    constructor(obj) {
        super(CheckpointEntryRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    static fromBuffer(buffer) {
        return CheckpointEntryRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, CheckpointEntryRecord)
    }

    static createInstance(checkpointId, value, timestamp) {
        let recordType = 'checkpointEntry'
        let now = new Date()
        let ID =  hashSortCoerce.hash([checkpointId, value, timestamp])
        const createdAt = Math.round(now.getTime() / 1000)
        return new CheckpointEntryRecord({ID, checkpointId, value, timestamp, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.checkpointentryrecord'
    }

}

module.exports = CheckpointEntryRecord