'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class SupplyChainRecord extends State {

    constructor(obj) {
        super(SupplyChainRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    static fromBuffer(buffer) {
        return SupplyChainRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    accept(acceptedActors) {
        let now = new Date();
        this.acceptedAt = Math.round(now.getTime() / 1000);
        this.acceptedActors = acceptedActors;
    }
    reject(){
        let now = new Date();
        this.rejectedAt = Math.round(now.getTime() / 1000);
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, SupplyChainRecord)
    }

    static createInstance(creator, productId, name, desc,  proposedActors, actorsOrder) {
        let recordType = 'supplyChain'
        let ID =  hashSortCoerce.hash([creator, productId, name, desc,  proposedActors, actorsOrder])
        let now = new Date()
        const proposedAt = Math.round(now.getTime() / 1000)
        let acceptedAt = null
        let acceptedActors = []
        let rejectedAt = null
        let checkpointsOrder = []
        let status = 'created'
        return new SupplyChainRecord({ ID, proposedActors, acceptedAt, acceptedActors, rejectedAt, actorsOrder, checkpointsOrder, status, recordType, proposedAt, name, desc, creator, productId })
    }

    static getClass() {
        return 'org.ftchain.supplychainrecord'
    }

}

module.exports = SupplyChainRecord