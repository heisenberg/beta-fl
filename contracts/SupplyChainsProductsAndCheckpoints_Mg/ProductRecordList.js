'use strict'

const StateList = require('./ledger-api/statelist.js')

const productRecord = require('./ProductRecord.js')

class ProductRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.productrecordlist')
        this.use(productRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = ProductRecordList