'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class ProductRecord extends State {

    constructor(obj) {
        super(ProductRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }

    static fromBuffer(buffer) {
        return ProductRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, ProductRecord)
    }

    static createInstance(name, desc, unit) {
        let recordType = 'product'
        let ID =  hashSortCoerce.hash([name, desc, unit])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new ProductRecord({ ID, name, desc, unit, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.productrecord'
    }

}

module.exports = ProductRecord