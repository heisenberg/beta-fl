'use strict'

const StateList = require('./ledger-api/statelist.js')

const supplyChainRecord = require('./SupplyChainRecord.js')

class SupplyChainRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.supplychainrecordlist')
        this.use(supplyChainRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = SupplyChainRecordList