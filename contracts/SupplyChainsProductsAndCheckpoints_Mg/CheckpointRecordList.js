'use strict'

const StateList = require('./ledger-api/statelist.js')

const checkpointRecord = require('./CheckpointRecord.js')

class CheckpointRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.checkpointrecordlist')
        this.use(checkpointRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = CheckpointRecordList