'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class ProductCycleIssueRecord extends State {

    constructor(obj) {
        super(ProductCycleIssueRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }

    getId() { return this.ID }
    getName() { return this.name }
    getDesc() { return this.desc }

    static fromBuffer(buffer) {
        return ProductCycleIssueRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, ProductCycleIssueRecord)
    }

    static createInstance(creator, responsiblePartyOrganization, responsiblePartyId, productCycleId, checkpointId, description, severity) {
        let recordType = 'productCycle'
        let ID =  hashSortCoerce.hash([creator, responsiblePartyOrganization, responsiblePartyId, productCycleId, checkpointId, description, severity])
        let now = new Date()
        const reportedAt = Math.round(now.getTime() / 1000)
        let fixed = false
        let finalReport = {}
        return new ProductCycleIssueRecord({ ID, creator, responsiblePartyOrganization, responsiblePartyId, productCycleId, checkpointId, description, severity, reportedAt, fixed, finalReportSteps: finalReport, recordType})
    }

    static getClass() {
        return 'org.ftchain.productcycleissuerecord'
    }

}

module.exports = ProductCycleIssueRecord