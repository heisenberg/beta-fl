'use strict'
const { Contract, Context } = require('fabric-contract-api')
const Utils = require('./Utils.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;

const CheckpointRecord = require('./CheckpointRecord.js')
const CheckpointRecordList = require('./CheckpointRecordList.js')
const CheckpointEntryRecord = require('./CheckpointEntryRecord.js')
const CheckpointEntryRecordList = require('./CheckpointEntryRecordList.js')
const ProductRecord = require('./ProductRecord.js')
const ProductRecordList = require('./ProductRecordList.js')
const SupplyChainRecord = require('./SupplyChainRecord.js')
const SupplyChainRecordList = require('./SupplyChainRecordList.js')
const ProductCycleRecord = require('./ProductCycleRecord.js')
const ProductCycleRecordList = require('./ProductCycleRecordList.js')
const ProductCycleIssueRecord = require('./ProductCycleIssueRecord.js')
const ProductCycleIssueRecordList = require('./ProductCycleIssueRecordList.js')


class SupplyChainsProductsAndCheckpointsContext extends Context {

    constructor() {
        super()
        this.CheckpointRecordList = new CheckpointRecordList(this)
        this.ProductRecordList = new ProductRecordList(this)
        this.SupplyChainRecordList = new SupplyChainRecordList(this)
        this.ProductCycleRecordList = new ProductCycleRecordList(this)
        this.ProductCycleIssueRecordList = new ProductCycleIssueRecordList(this)
        this.CheckpointEntryRecordList = new CheckpointEntryRecordList(this)
    }
}

/**
 *
 */
class SupplyChainsProductsAndCheckpointsContract extends Contract {

    constructor() {
        super('org.ftchain.supplychainsproductsandcheckpointscontract')
    }

    createContext() {
        return new SupplyChainsProductsAndCheckpointsContext()
    }

    async init(ctx) {
        console.log('Instantiating the smart contract that manages all FTCHAIN users & devices.')
        console.log('(1) Creating Supply Chain ROLES ------- BEGIN')

        let name = 'Seed Seller'
        let desc = 'Person/entity that collects, packages and sells seeds.'
        let record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Farmer'
        desc = 'Person/entity who owns or manages a farm.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Distributor'
        desc = 'Person/entity who supplies goods to retailers.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Processor'
        desc = 'Person/entity engaged in processing agricultural products and preparing them for market.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Storage Owner'
        desc = 'Person/entity capable of storing produce/products long/short term in between other processes.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Wholesaler'
        desc = 'Person/entity that sells goods in large quantities at low prices, typically to retailers.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Retailer'
        desc = 'Person/entity that sells goods to the public in relatively small quantities for use or consumption rather than for resale.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        console.log('(1) Creating Supply Chain ROLES ------- END')

    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction: execution results-----------------------')
        console.log(results)
    }


    /**
     * Create a new supply chain record
     *
     * @param {Context} ctx the transaction context
     * @param {String} productId the BC ID of the unique product the supply chain is for
     * @param {String} name the name of the supply chain
     * @param {String} desc ad-hoc description
     * @param {String} proposedActors the array of other BC SupplyChainActors/Wholesalers/Retailers IDs (passed as a string)
     * @param {String} actorsOrder the array of BC SupplyChainActors/Wholesalers/Retailers IDs (passed as a string) in order they need to appear in the supply chain
     *
     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createSupplyChainRecords(ctx, productId, name, desc,  proposedActors, actorsOrder) {
        try {

            let record = SupplyChainRecord.createInstance(ctx.enrollmentID, productId, name, desc,  JSON.parse(proposedActors), JSON.parse(actorsOrder))
            await ctx.SupplyChainRecordList.addRecord(record)
            console.log(`Created SupplyChain record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createSupplyChainRecords function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    // accept - reject supply chain
    async modifySupplyChain(ctx, supplyChainID, name, desc,  proposedActors, actorsOrder) {
        try {

            let record = SupplyChainRecord.createInstance(ctx.enrollmentID, productId, name, desc,  JSON.parse(proposedActors), JSON.parse(actorsOrder))
            await ctx.SupplyChainRecordList.addRecord(record)
            console.log(`Created SupplyChain record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createSupplyChainRecords function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new Product record
     *
     * @param {Context} ctx the transaction context
     * @param {String} name the name of product
     * @param {String} desc the ad-hoc description
     * @param {String} unit the unit of the product (e.g. kg, piece)

     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createProductRecord(ctx, name, desc, unit) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }
            let record = ProductRecord.createInstance(name, desc, unit)
            await ctx.ProductRecordList.addRecord(record)
            console.log(`Created Product record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createProductRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new Product record
     *
     * @param {Context} ctx the transaction context
     * @param {String} name the name of product
     * @param {String} desc the ad-hoc description
     * @param {String} unit the unit of the product (e.g. kg, piece)

     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createProductCycleRecord(ctx, name, desc, unit) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }
            let record = ProductRecord.createInstance(name, desc, unit)
            await ctx.ProductRecordList.addRecord(record)
            console.log(`Created Product record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createProductRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new checkpoint record
     *
     * @param {Context} ctx the transaction context
     * @param {String} deviceId the BC ID of the device
     * @param {String} supplyChainId the ID of the already created supply chain
     * @param {String} deviceId the ID of the already created device record
     * @param {String} sensorId the ID of the already created sensor record
     * @param {String} name the supply chain name
     * @param {String} description supply chain desc
     *
     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createCheckpointRecord(ctx, supplyChainId, deviceId, sensorId, name, description) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            //TODO: Check existance of SC, device, sensor before proceeding
            let record = CheckpointRecord.createInstance(ctx.enrollmentId, supplyChainId, deviceId, sensorId, name, description)
            await ctx.CheckpointRecordList.addRecord(record)
            console.log(`Created Checkpoint record: ${JSON.stringify(record)}`)
            return record

        } catch (e) {
            let message = `Error in createCheckpointRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new checkpoint record entry
     *
     * @param {Context} ctx the transaction context
     * @param {String} checkpointId the BC ID of the checkpoint
     * @param {String} value the checkpoint/sensor value
     * @param {String} timestamp the time
     *
     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createCheckpointEntryRecord(ctx, checkpointId, value, timestamp) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }
            //TODO: Check existance of checkpoint
            let record = CheckpointEntryRecord.createInstance(ctx.enrollmentId, checkpointId, value, timestamp)
            await ctx.CheckpointEntryRecordList.addRecord(record)
            console.log(`Created CheckpointEntry record: ${JSON.stringify(record)}`)
            return record

        } catch (e) {
            let message = `Error in createCheckpointEntryRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Evaluate a queryString. This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     *
     * @returns {JSON}  the records for queryString
     * @throws {shim.error} transaction execution failure
     */
    async queryWithQueryString(ctx, queryString) {
        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }

    /**
     * Read operation: Users
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllUsers(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'user'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllUsers function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Users
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMyRecord(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'user',
                    "owner": ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults[0]
        } catch (e) {
            let message = `Error in getMyRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllDevices(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'device'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevices function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     * @param {String} status the status of device
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllDevicesForStatus(ctx, status) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'device',
                    'status': status
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevicesForStatus function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMyDevices(ctx) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            let queryString = {
                "selector": {
                    "recordType": 'device',
                    'owner': ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevices function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllSensors(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Operation forbidden.`)

            let queryString = {
                "selector": {
                    "recordType": 'sensor'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllSensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     * @param {String} status the status of sensor
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllSensorsForStatus(ctx, status) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Operation forbidden.`)

            let queryString = {
                "selector": {
                    "recordType": 'sensor',
                    'status': status
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllSensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMySensors(ctx) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            let queryString = {
                "selector": {
                    "recordType": 'sensor',
                    'owner': ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getMySensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
}

module.exports = SupplyChainsProductsAndCheckpointsContract