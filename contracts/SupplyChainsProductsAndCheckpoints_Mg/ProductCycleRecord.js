'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class ProductCycleRecord extends State {

    constructor(obj) {
        super(ProductCycleRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }

    getId() { return this.ID }
    getName() { return this.name }
    getDesc() { return this.desc }

    static fromBuffer(buffer) {
        return ProductCycleRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, ProductCycleRecord)
    }

    static createInstance(currentOwner, supplyChainId, productInfo) {
        let recordType = 'productCycle'
        let checkpointsVerified = []
        let ID =  hashSortCoerce.hash([currentOwner, supplyChainId, productInfo])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new ProductCycleRecord({ ID, currentOwner, supplyChainId, checkpointsVerified, productInfo, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.productcyclerecord'
    }

}

module.exports = ProductCycleRecord