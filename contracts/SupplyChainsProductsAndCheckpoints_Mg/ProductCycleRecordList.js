'use strict'

const StateList = require('./ledger-api/statelist.js')

const pCycleRecord = require('./ProductCycleRecord.js')

class ProductCycleRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.productcyclerecordlist')
        this.use(pCycleRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = ProductCycleRecordList