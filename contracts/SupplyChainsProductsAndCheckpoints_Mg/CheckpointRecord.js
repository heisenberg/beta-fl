'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class CheckpointRecord extends State {

    constructor(obj) {
        super(CheckpointRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    static fromBuffer(buffer) {
        return CheckpointRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, CheckpointRecord)
    }

    static createInstance(owner, supplyChainId, deviceId, sensorId, name, description) {
        let recordType = 'checkpoint'
        let now = new Date()
        let ID =  hashSortCoerce.hash([owner, supplyChainId, deviceId, sensorId, name, description])
        const createdAt = Math.round(now.getTime() / 1000)
        return new CheckpointRecord({ID, owner, supplyChainId, deviceId, sensorId, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.checkpointrecord'
    }

}

module.exports = CheckpointRecord