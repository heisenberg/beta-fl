'use strict'

const StateList = require('./ledger-api/statelist.js')

const pCycleIssueRecord = require('./ProductCycleIssueRecord.js')

class ProductCycleIssueRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.productcycleissuerecordlist')
        this.use(pCycleIssueRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = ProductCycleIssueRecordList