'use strict'
const { Contract, Context } = require('fabric-contract-api')
const RetailerRecord = require('./retailerRecord.js')
const Utils = require('./utils.js')
const RetailersRecordList = require('./retailersRecordList.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;

class RetailerContext extends Context {

    constructor() {
        super()
        this.retailersRecordList = new RetailersRecordList(this)
    }

}

/**
 * Define patient record smart contract by extending Fabric Contract class
 *
 */
class RetailerContract extends Contract {

    constructor() {
        super('org.foodsupplychain.retailerscontract')
    }

    /**
     * Define a custom context for commercial paper
     */
    createContext() {
        return new RetailerContext()
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async init(ctx) {
        console.log('Instantiated the retailer smart contract.')

    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction-----------------------')
        console.log(results)
    }


    async createRetailerRecord(ctx, owner, name) {
        try {
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let record = RetailerRecord.createInstance(owner, name)
            await ctx.retailersRecordList.addRecord(record)
            console.log(`Created retailer record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createRetailerRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }


    }

    async updateRetailer(ctx, name) {
        try {
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let record
            try {
                let key = RetailerRecord.makeKey([ctx.enrollmentID])
                record = await ctx.retailersRecordList.getRecord(key)
            } catch (e) {
                return shim.error(`Retailer record not found`)
            }
            console.log(`Retailer by partial key found: ${JSON.stringify(record)}`)
            record.setName(name)
            await ctx.retailersRecordList.updateRecord(record)
            console.log(`Retailer updated: ${JSON.stringify(record)}`)
            return record

        } catch (e) {
            let message = `Error in updateRetailer function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     */
    async queryWithQueryString(ctx, queryString) {
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }


    async getAllRetailers(ctx) {
        try {
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let queryString = {
                "selector": {
                    "recordType": 'retailer'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getRetailers function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getMySelf(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'retailer',
                    "owner": ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults[0]
        } catch (e) {
            let message = `Error in getMySelf function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
}
module.exports = RetailerContract