'use strict'
const State = require('./ledger-api/state.js')
const { v4: uuidv4 } = require('uuid');

class RetailerRecord extends State {

    constructor(obj) {
        super(RetailerRecord.getClass(), [obj.owner])
        Object.assign(this, obj)
    }


    getName() { return this.name }
    getOwner() { return this.owner }
    getCreatedAt() { return this.createdAt }

    setName(name) { this.name = name }

    static fromBuffer(buffer) {
        return RetailerRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, RetailerRecord)
    }

    static createInstance(owner, name) {
        let recordType = 'retailer'
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new RetailerRecord({ name, owner, recordType, createdAt })
    }

    static getClass() {
        return 'org.foodsupplychain.retailerrecord'
    }

}

module.exports = RetailerRecord