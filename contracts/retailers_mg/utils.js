function dumpError(err) {
    if (typeof err === 'object') {
        if (err.message) {
            console.log('\nMessage: ' + err.message)
        }
        if (err.stack) {
            console.log('\nStacktrace:')
            console.log('====================')
            console.log(err.stack);
        }
    } else {
        console.log('dumpError :: argument is not an object');
    }
}

module.exports.dumpError = dumpError
// Organizations constants
module.exports.soPMSP = "serviceprovidersMSP"
module.exports.procMSP = "processorsMSP"
module.exports.govMSP = "governmentMSP"
module.exports.consMSP = "consumersMSP"
module.exports.wholeSMSP = "wholesalersMSP"
module.exports.prodMSP = "producersMSP"
module.exports.distMSP = "distributorsMSP"
module.exports.extActMSP = "externalActorsMSP"
module.exports.storOwnMSP = "storageOwnersMSP"
module.exports.platoMSP = "platformoperatorMSP"
module.exports.reatilerMSP = "retailersMSP"

