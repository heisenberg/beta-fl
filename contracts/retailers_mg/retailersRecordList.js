'use strict'

const StateList = require('./ledger-api/statelist.js')

const retailerRecord = require('./retailerRecord.js')

class RetailersRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.foodsupplychain.retailersrecordlist')
        this.use(retailerRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = RetailersRecordList