'use strict'

const StateList = require('./ledger-api/statelist.js')

const modelsRecord = require('./modelsRecord.js')

class ModelsRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.foodsupplychain.modelsRecordList')
        this.use(modelsRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = ModelsRecordList