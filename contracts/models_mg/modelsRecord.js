'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class ModelsRecord extends State {

    constructor(obj) {
        super(ModelsRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    getCID() { return this.CID }
    getFlag() { return this.flag }
    getExperimentID() { return this.experimentID }
    getRoundNo() { return this.roundNo }
    getNumberOfExamples() { return this.numberOfExamples }
    getOwner() { return this.owner }
    getCreatedAt() { return this.createdAt }

    setName(name) { this.name = name }
    setExperimentID(experimentID) { this.experimentID = experimentID }
    setCID(CID) { this.CID = CID }
    setRoundNo(roundNo) { this.roundNo = roundNo }
    setFlag(flag) { this.flag = flag }
    setNumberOfExamples(numberOfExamples) { this.numberOfExamples = numberOfExamples }

    static fromBuffer(buffer) {
        return ModelsRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, ModelsRecord)
    }

    static createInstance(owner, recordType,CID, roundNo, numberOfExamples,experimentID) {

        let ID =  hashSortCoerce.hash([owner, CID, roundNo, numberOfExamples,experimentID])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new ModelsRecord({ ID, owner,recordType, CID, roundNo, numberOfExamples, experimentID,createdAt})
    }

    static getClass() {
        return 'org.foodsupplychain.modelsrecord'
    }

}

module.exports = ModelsRecord