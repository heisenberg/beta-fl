'use strict'
const { Contract, Context } = require('fabric-contract-api')
const ModelsRecord = require('./modelsRecord.js')
const Utils = require('./utils.js')
const ModelsRecordList = require('./modelsRecordList.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class ModelsContext extends Context {

    constructor() {
        super()
        this.modelsRecordList = new ModelsRecordList(this)
    }
}

class ModelsContract extends Contract {

    constructor() {
        super('org.foodsupplychain.modelscontract')
    }

    createContext() {
        return new ModelsContext()
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async init(ctx) {
        console.log('Instantiated the models smart contract.')

    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction-----------------------')
        console.log(results)
    }

    async uploadLocalModel(ctx, CID,roundNo,numberOfExamples, experimentID) {
        try {
            let recordType = 'localmodels'
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let ID =  hashSortCoerce.hash([ctx.enrollmentID, CID, roundNo, numberOfExamples,experimentID])
            let modelExist = await this.getByID(ctx, ID)
            if (modelExist !== 0){
                return shim.error("Model with this parameters already exists")
            }
            let record = ModelsRecord.createInstance(ctx.enrollmentID,recordType, CID, roundNo,numberOfExamples,experimentID)
            await ctx.modelsRecordList.addRecord(record)
            console.log(`Created new local model: ${JSON.stringify(record)}`)
            console.log("Information get is OK")
            await ctx.stub.setEvent('local-model-invoke', Buffer.from(JSON.stringify(record)))
            console.log('Set event fired')
            return record
        } catch (e) {
            let message = `Error in create model function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async uploadGlobalModel(ctx, CID,roundNo,experimentID) {
        try {
            let recordType = 'globalmodels'
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
            //    if (ctx.mspID !== Utils.Consumers) {
            //      return shim.error(`Function private to ${Utils.Consumers}.`)
            //}
            let record = ModelsRecord.createInstance(ctx.enrollmentID,recordType, CID, roundNo,'',experimentID)
            await ctx.modelsRecordList.addRecord(record)
            console.log(`Created new Global model: ${JSON.stringify(record)}`)
            const assetBuffer = Buffer.from(JSON.stringify(record));
            await ctx.stub.setEvent('global-model-invoke', assetBuffer);
            return record
        } catch (e) {
            let message = `Error in create model function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    async updateVersion(ctx, ID, version) {
        try {
            let record
            try {
                let key = ModelsRecord.makeKey([ID])
                record = await ctx.modelsRecordList.getRecord(key)
            } catch (e) {
                return shim.error(`model record not found`)
            }

            console.log(`model by partial key found: ${JSON.stringify(record)}`)
            record.setVersion(version)
            await ctx.modelRecordList.updateRecord(record)
            console.log(`model updated: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in updateVersion function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     */
    async queryWithQueryString(ctx, queryString) {
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }


    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     * @param {int} pageSize for pagination
     */
    async queryWithQueryStringWithPagination(ctx, queryString, pageSize) {
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

        let resultsIterator = ctx.stub.getQueryResultWithPagination(queryString, pageSize,'');
        console.log(typeof(resultsIterator))
        console.log(resultsIterator)
        let allResults = [];
        for await (const res of resultsIterator) {
            allResults.push(JSON.parse(res.value.toString('utf8')));
        }
        return allResults
    }



    // Get Models for Round numbers)

    async getLocalModelsForRoundNumber(ctx, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "roundNo": roundNumber

                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults;
        } catch (e) {
            let message = `Error in getLocalModelsForRoundNumber function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsNumberForRoundNumber(ctx, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "roundNo": roundNumber

                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getLocalModelsNumberForRoundNumber function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsNumberForRoundNumber(ctx, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getGlobalModelsNumberForRoundNumber function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsForRoundNumber(ctx, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getGlobalModelsForRoundNumber function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsFilter(ctx, experimentID, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults;
        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsFilterPagination(ctx, experimentID, roundNumber, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "desc"}]
            }
          return await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);

        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsPagination(ctx, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                },
                "sort": [{"createdAt": "desc"}]
            }
            return await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);

        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsPagination(ctx, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                },
                "sort": [{"createdAt": "desc"}]
            }
            return await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);

        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsFilter(ctx, experimentID, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults;
        } catch (e) {
            let message = `Error in getGlobalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsFilterPagination(ctx, experimentID, roundNumber, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "desc"}]
            }
            return await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);

        } catch (e) {
            let message = `Error in getGlobalModelsFilterPagination function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsFilterLength(ctx, experimentID, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getByID(ctx, ID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "ID": ID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getLocalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsFilterLength(ctx, experimentID, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getGlobalModelsFilter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
    // Get models for round ID //
    async getLocalModelsForRoundID(ctx, experimentID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "experimentID": experimentID
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults;
        } catch (e) {
            let message = `Error in getLocalModelsForRoundID function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLocalModelsNumberForRoundID(ctx, experimentID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels',
                    "experimentID": experimentID

                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getLocalModelsNumberForRoundID function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsNumberForRoundID(ctx, experimentID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "experimentID": experimentID

                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults.length;
        } catch (e) {
            let message = `Error in getGlobalModelsNumberForRoundID function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getGlobalModelsForRoundID(ctx, experimentID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels',
                    "experimentID": experimentID
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getGlobalModelsForRoundID function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getAllLocalModels(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'localmodels'
                },
                "sort": [{"createdAt": "asc"}]
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllModels function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getAllGlobalModels(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'globalmodels'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllModels function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
}
module.exports = ModelsContract