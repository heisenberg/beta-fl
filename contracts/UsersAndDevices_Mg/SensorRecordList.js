'use strict'

const StateList = require('./ledger-api/statelist.js')

const sensorRecord = require('./SensorRecord.js')

class SensorRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.sensorrecordlist')
        this.use(sensorRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = SensorRecordList