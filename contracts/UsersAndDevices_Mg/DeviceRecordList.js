'use strict'

const StateList = require('./ledger-api/statelist.js')

const deviceRecord = require('./DeviceRecord.js')

class DeviceRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.devicerecordlist')
        this.use(deviceRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = DeviceRecordList