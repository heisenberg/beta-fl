'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class DeviceRecord extends State {

    constructor(obj) {
        super(DeviceRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    static fromBuffer(buffer) {
        return DeviceRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, DeviceRecord)
    }

    static createInstance(owner, MAC, serialNo, desc, status) {
        let recordType = 'device'
        let ID =  hashSortCoerce.hash([owner, MAC, serialNo, desc, status])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new DeviceRecord({ ID, owner, MAC, serialNo, desc, status, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.devicerecord'
    }

}

module.exports = DeviceRecord