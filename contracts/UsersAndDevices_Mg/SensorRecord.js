'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class SensorRecord extends State {

    constructor(obj) {
        super(SensorRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }

    getId() { return this.ID }
    getName() { return this.name }
    getDesc() { return this.desc }

    static fromBuffer(buffer) {
        return SensorRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, SensorRecord)
    }

    // If sensorBoard=true -> type=[], measurementUnit=[]
    static createInstance(owner, deviceId, serialNo, type, measurementUnit, status, sensorBoard) {
        let recordType = 'sensor'
        let ID =  hashSortCoerce.hash([owner, deviceId, serialNo, type, measurementUnit, status, sensorBoard])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new SensorRecord({ ID, owner, deviceId, serialNo, type, measurementUnit, status, sensorBoard, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.sensorrecord'
    }

}

module.exports = SensorRecord