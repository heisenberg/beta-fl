'use strict'
const { Contract, Context } = require('fabric-contract-api')
const Utils = require('./Utils.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;

const UserRecord = require('./UserRecord.js')
const UserRecordList = require('./UserRecordList.js')
const RoleRecord = require('./RoleRecord.js')
const RoleRecordList = require('./RoleRecordList.js')
const DeviceRecord = require('./DeviceRecord.js')
const DeviceRecordList = require('./DeviceRecordList.js')
const SensorRecord = require('./SensorRecord.js')
const SensorRecordList = require('./SensorRecordList.js')

class UsersAndDevicesContext extends Context {

    constructor() {
        super()
        this.UserRecordList = new UserRecordList(this)
        this.RoleRecordList = new RoleRecordList(this)
        this.DeviceRecordList = new DeviceRecordList(this)
        this.SensorRecordList = new SensorRecordList(this)
    }
}

/**
 *
 */
class UsersAndDevicesContract extends Contract {

    constructor() {
        super('org.ftchain.usersanddevicescontract')
    }

    createContext() {
        return new UsersAndDevicesContext()
    }

    async init(ctx) {
        console.log('Instantiating the smart contract that manages all FTCHAIN users & devices.')
        console.log('(1) Creating Supply Chain ROLES ------- BEGIN')

        let name = 'Seed Seller'
        let desc = 'Person/entity that collects, packages and sells seeds.'
        let record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Farmer'
        desc = 'Person/entity who owns or manages a farm.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Distributor'
        desc = 'Person/entity who supplies goods to retailers.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Processor'
        desc = 'Person/entity engaged in processing agricultural products and preparing them for market.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Storage Owner'
        desc = 'Person/entity capable of storing produce/products long/short term in between other processes.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Wholesaler'
        desc = 'Person/entity that sells goods in large quantities at low prices, typically to retailers.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        name = 'Retailer'
        desc = 'Person/entity that sells goods to the public in relatively small quantities for use or consumption rather than for resale.'
        record = RoleRecord.createInstance(name, desc)
        await ctx.RoleRecordList.addRecord(record)

        console.log('(1) Creating Supply Chain ROLES ------- END')
    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction: execution results-----------------------')
        console.log(results)
    }


    /**
     * Create a new user record
     *
     * @param {Context} ctx the transaction context
     * @param {String} name the full name of the user
     * @param {String} company the company name of the user
     * @param {String} address user/company location/address
     * @param {String} roles the array of role IDs (passed as a string)

     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createUserRecord(ctx, name, company, address, roles) {
        try {
            //TODO: Check if passed roles exist
            let record = UserRecord.createInstance(ctx.enrollmentID, name, company, address, JSON.parse(roles))
            await ctx.UserRecordList.addRecord(record)
            console.log(`Created User record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createUserRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new device record
     *
     * @param {Context} ctx the transaction context
     * @param {String} MAC the mac address of the device
     * @param {String} serialNo the unique serial number of the device
     * @param {String} desc the description of the device (ad-hoc)
     * @param {String} status the current operational status (created/active/paused/decommissioned)

     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createDeviceRecord(ctx, MAC, serialNo, desc, status) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }
            let record = DeviceRecord.createInstance(ctx.enrollmentID, MAC, serialNo, desc, status)
            await ctx.DeviceRecordList.addRecord(record)
            console.log(`Created Device record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in createDeviceRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Create a new sensor record
     *
     * @param {Context} ctx the transaction context
     * @param {String} deviceId the BC ID of the device
     * @param {String} serialNo the unique serial number of the device
     * @param {String} type the type of sensor; must belong to Utils.allowedSensorTypes. Array if sensorBoard=true.
     * @param {String} measurementUnit the unit sensor measures in (C, F, etc.). Array if sensorBoard=true.
     * @param {String} status the current operational status (created/active/paused/decommissioned)
     * @param {String} sensorBoard true for sensorBoards, false if single sensor
     *
     * @returns {JSON}  the created record
     * @throws {shim.error} transaction execution failure
     */
    async createSensorRecord(ctx, deviceId, serialNo, type, measurementUnit, status, sensorBoard) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            var sensorBoardObj = (sensorBoard === 'true');

            if (!sensorBoardObj) {
                if (!(Utils.existsInArray(Utils.allowedSensorTypes, type)))
                    return shim.error(`Sensor type not allowed.`)

                let record = SensorRecord.createInstance(ctx.enrollmentID, deviceId, serialNo, type, measurementUnit, status, sensorBoardObj)
                await ctx.SensorRecordList.addRecord(record)
                console.log(`Created Sensor record: ${JSON.stringify(record)}`)
                return record
            }
            else {
                var typeObj = JSON.parse(type)
                var measurementUnitObj = JSON.parse(measurementUnit)
                for (const type of typeObj) {
                    if (!(Utils.existsInArray(Utils.allowedSensorTypes, type)))
                        return shim.error(`Sensor type not allowed.`)
                }
                let record = SensorRecord.createInstance(ctx.enrollmentID, deviceId, serialNo, typeObj, measurementUnitObj, status, sensorBoardObj)
                await ctx.SensorRecordList.addRecord(record)
                console.log(`Created Sensor record: ${JSON.stringify(record)}`)
                return record
            }

        } catch (e) {
            let message = `Error in createDeviceRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Evaluate a queryString. This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     *
     * @returns {JSON}  the records for queryString
     * @throws {shim.error} transaction execution failure
     */
    async queryWithQueryString(ctx, queryString) {
        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }

    /**
     * Read operation: Users
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllUsers(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'user'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllUsers function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Users
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMyRecord(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'user',
                    "owner": ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults[0]
        } catch (e) {
            let message = `Error in getMyRecord function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllDevices(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'device'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevices function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     * @param {String} status the status of device
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllDevicesForStatus(ctx, status) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)

            let queryString = {
                "selector": {
                    "recordType": 'device',
                    'status': status
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevicesForStatus function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    /**
     * Read operation: Devices
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMyDevices(ctx) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            let queryString = {
                "selector": {
                    "recordType": 'device',
                    'owner': ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllDevices function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllSensors(ctx) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Operation forbidden.`)

            let queryString = {
                "selector": {
                    "recordType": 'sensor'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllSensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     * @param {String} status the status of sensor
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getAllSensorsForStatus(ctx, status) {
        try {
            if (!(ctx.mspID === Utils.PlatformOperator))
                return shim.error(`Operation forbidden.`)

            let queryString = {
                "selector": {
                    "recordType": 'sensor',
                    'status': status
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllSensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Read operation: Sensors
     *
     * @param {Context} ctx the transaction context
     *
     * @returns {JSON}  the results as array
     * @throws {shim.error} transaction execution failure
     */
    async getMySensors(ctx) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            let queryString = {
                "selector": {
                    "recordType": 'sensor',
                    'owner': ctx.enrollmentID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getMySensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getSensorsForDevice(ctx, deviceId) {
        try {
            if (!( Utils.existsInArray (Utils.canCreateDevices, ctx.mspID))){
                return shim.error(`Function forbidden for caller from ${ctx.mspID}.`)
            }

            let queryString = {
                "selector": {
                    "recordType": 'sensor',
                    'deviceId': deviceId
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getSensorsForDevice function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getAllRoles(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'role',
                }
            }
            return await this.queryWithQueryString(ctx, JSON.stringify(queryString));
        } catch (e) {
            let message = `Error in getMySensors function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
}

module.exports = UsersAndDevicesContract