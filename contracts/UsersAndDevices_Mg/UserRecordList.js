'use strict'

const StateList = require('./ledger-api/statelist.js')

const userRecord = require('./UserRecord.js')

class UserRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.userrecordlist')
        this.use(userRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = UserRecordList