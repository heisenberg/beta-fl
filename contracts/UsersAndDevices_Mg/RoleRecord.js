'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class RoleRecord extends State {

    constructor(obj) {
        super(RoleRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }

    getId() { return this.ID }
    getName() { return this.name }
    getDesc() { return this.desc }

    static fromBuffer(buffer) {
        return RoleRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, RoleRecord)
    }

    static createInstance(name, desc) {
        let recordType = 'role'
        let ID =  hashSortCoerce.hash([name, desc])
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new RoleRecord({ ID, name, desc, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.rolerecord'
    }

}

module.exports = RoleRecord