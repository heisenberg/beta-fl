'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class UserRecord extends State {

    constructor(obj) {
        super(UserRecord.getClass(), [obj.ID, obj.owner])
        Object.assign(this, obj)
    }

    addRole(roleId){
        self.roles.push(roleId)
        return self.roles
    }

    removeRole (roleId){
        const index = self.roles.indexOf(roleId);
        if (index > -1) {
            self.roles.splice(index, 1);
        }
        return self.roles
    }

    static fromBuffer(buffer) {
        return UserRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, UserRecord)
    }

    static createInstance(owner, name, company, address, roles) {
        let recordType = 'user'
        let now = new Date()
        let ID =  hashSortCoerce.hash([owner, name, company, address, roles])
        const createdAt = Math.round(now.getTime() / 1000)
        return new UserRecord({ID, owner, name, company, address, roles, recordType, createdAt })
    }

    static getClass() {
        return 'org.ftchain.userrecord'
    }

}

module.exports = UserRecord