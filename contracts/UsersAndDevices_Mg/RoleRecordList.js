'use strict'

const StateList = require('./ledger-api/statelist.js')

const roleRecord = require('./RoleRecord.js')

class RoleRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.ftchain.rolerecordlist')
        this.use(roleRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = RoleRecordList