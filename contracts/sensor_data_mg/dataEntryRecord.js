'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class DataEntryRecord extends State {

    constructor(obj) {
        super(DataEntryRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    // getName() { return this.name }
    // getOwner() { return this.owner }
    // getCreatedAt() { return this.createdAt }
    //
    // setName(name) { this.name = name }

    static fromBuffer(buffer) {
        return DataEntryRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, DataEntryRecord)
    }

    static createInstance(owner, deviceID, epochTimeUTC, sensorData) {
        let recordType = 'dataEntry'
        let ID =  hashSortCoerce.hash([owner, deviceID, epochTimeUTC, sensorData])
        return new DataEntryRecord({ ID, owner, deviceID, epochTimeUTC, sensorData, recordType})
    }

    static getClass() {
        return 'org.foodsupplychain.dataentryrecord'
    }

}

module.exports = DataEntryRecord