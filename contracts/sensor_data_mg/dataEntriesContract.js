'use strict'
const { Contract, Context } = require('fabric-contract-api')
const DataEntryRecord = require('./dataEntryRecord.js')
const Utils = require('./utils.js')
const DataEntriesRecordList = require('./dataEntriesRecordList.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;

class DataEntriesContext extends Context {

    constructor() {
        super()
        this.dataEntriesRecordList = new DataEntriesRecordList(this)
    }

}

class DataEntriesContract extends Contract {

    constructor() {
        super('org.foodsupplychain.dataentriescontract')
    }

    /**
     * Define a custom context for commercial paper
     */
    createContext() {
        return new DataEntriesContext()
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async init(ctx) {
        console.log('Instantiated the retailer smart contract.')

    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction-----------------------')
        console.log(results)
    }


    async insertDataEntry(ctx, deviceID, epochTimeUTC, sensorData) {
        try {

            let record = DataEntryRecord.createInstance(ctx.enrollmentID, deviceID, epochTimeUTC, sensorData)
            await ctx.dataEntriesRecordList.addRecord(record)
            console.log(`Created data entry record: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in insertDataEntry function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     */
    async queryWithQueryString(ctx, queryString) {
        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }

    async getAllEntries(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'dataEntry'
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllEntriesForDevice function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getAllEntriesForDevice(ctx, deviceID) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'dataEntry',
                    'deviceID': deviceID
                }
            }
            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults
        } catch (e) {
            let message = `Error in getAllEntriesForDevice function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }
}
module.exports = DataEntriesContract