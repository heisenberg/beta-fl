'use strict'

const StateList = require('./ledger-api/statelist.js')

const dataEntryRecord = require('./dataEntryRecord.js')

class DataEntriesRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.foodsupplychain.dataentriesrecordlist')
        this.use(dataEntryRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = DataEntriesRecordList