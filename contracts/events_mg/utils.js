 function dumpError(err) {
    if (typeof err === 'object') {
        if (err.message) {
            console.log('\nMessage: ' + err.message)
        }
        if (err.stack) {
            console.log('\nStacktrace:')
            console.log('====================')
            console.log(err.stack);
        }
    } else {
        console.log('dumpError :: argument is not an object');
    }
}

module.exports.dumpError = dumpError
// Organizations constants
module.exports.SupplyChainActors = "SupplyChainActorsMSP"
module.exports.Wholesalers = "WholesalersMSP"
module.exports.Retailers = "RetailersMSP"
module.exports.AiServiceProviders = "AiServiceProvidersMSP"
module.exports.PlatformOperator = "PlatformOperatorMSP"
module.exports.ExternalParties = "ExternalPartiesMSP"
module.exports.Consumers = "ConsumersMSP"
