'use strict'
const { Contract, Context } = require('fabric-contract-api')
const EventsRecord = require('./eventsRecord.js')
const Utils = require('./utils.js')
const EventsRecordList = require('./eventsRecordList.js')
const shim = require('fabric-shim');
const ClientIdentity = shim.ClientIdentity;

class EventsContext extends Context {

    constructor() {
        super()
        this.eventsRecordList = new EventsRecordList(this)
    }
}

class EventsContract extends Contract {

    constructor() {
        super('org.foodsupplychain.eventscontract')
    }

    createContext() {
        return new EventsContext()
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async init(ctx) {
        console.log('Instantiated the events smart contract.')

    }

    async unknownTransaction(ctx) {
        return shim.error('Function name missing')
    }

    async beforeTransaction(ctx) {
        console.log('---------------------beforeTransaction-----------------------')
        let func_and_params = ctx.stub.getFunctionAndParameters()
        console.log('---------------------func_and_params-----------------------')
        console.log(func_and_params)
        let cid = new ClientIdentity(ctx.stub);
        console.log('---------------------MSPID-----------------------')
        console.log(cid.getMSPID())
        console.log('---------------------ATTR_ENROLLMENT_ID-----------------------')
        console.log(cid.getAttributeValue('hf.EnrollmentID'))
        console.log('---------------------FUNCTION EXECUTION START-----------------------')
        ctx.enrollmentID = cid.getAttributeValue('hf.EnrollmentID')
        ctx.mspID = cid.getMSPID()
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
    }

    async afterTransaction(ctx, results) {
        console.log('---------------------afterTransaction-----------------------')
        console.log(results)
    }

    async eventUpdate(ctx,roundNo,experimentID,modelsLen) {
        try {

            if (parseInt(roundNo) < 0){
                return shim.error("Round number must be 0 or greater")
            }

            let recordType = 'events'
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let getLastEventRecord = await this.getLastEventQuery(ctx, experimentID, roundNo)
            if (getLastEventRecord.length !== 0) {
                if (parseInt(getLastEventRecord[0].Record.roundNo) >=  parseInt(roundNo)){
                    return shim.error("Round number can only be updated to bigger number")
                }
            }

            let record = EventsRecord.createInstance( roundNo,experimentID, modelsLen, recordType )
            console.log("Event record created on BCH")
            await ctx.eventsRecordList.addRecord(record)
            console.log(`Created new event: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in eventUpdate function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    async eventUpdateUserCheck(ctx,roundNo,experimentID,modelsLen) {
        try {
            let recordType = 'events'
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)
            //    if (ctx.mspID !== Utils.Consumers) {
            //      return shim.error(`Function private to ${Utils.Consumers}.`)
            //}
            let record = EventsRecord.createInstance( roundNo,experimentID, modelsLen, recordType )
            await ctx.eventsRecordList.addRecord(record)
            console.log(`Created new event: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in evenet update function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async updateCounter(ctx, ID, counter) {
        try {
            console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

            let record
            try {
                let key = EventsRecord.makeKey([ID])
                record = await ctx.eventsRecordList.getRecord(key)
            } catch (e) {
                return shim.error(`Event record not found`)
            }
            console.log(`Event by partial key found: ${JSON.stringify(record)}`)
            record.setCounter(counter)
            await ctx.eventsRecordList.updateRecord(record)
            console.log(`Event updated: ${JSON.stringify(record)}`)
            return record
        } catch (e) {
            let message = `Error in updateCounter function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     */
    async queryWithQueryString(ctx, queryString) {
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

        let resultsIterator = await ctx.stub.getQueryResult(queryString);
        let allResults = [];
        while (true) {
            let res = await resultsIterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                await resultsIterator.close();
                return allResults
            }
        }
        return allResults
    }

    /**
     * Evaluate a queryString
     * This is the helper function for making queries using a query string
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
     * @param {int} pageSize for pagination
     */
    async queryWithQueryStringWithPagination(ctx, queryString, pageSize) {
        console.log(`Caller MSP: ${ctx.mspID}, Caller username: ${ctx.enrollmentID}`)

        let resultsIterator = ctx.stub.getQueryResultWithPagination(queryString, pageSize,'');
        console.log(typeof(resultsIterator))
        console.log(resultsIterator)
        let allResults = [];
        for await (const res of resultsIterator) {
            allResults.push(JSON.parse(res.value.toString('utf8')));
        }
        return allResults
    }

    async getLastEvent(ctx, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'events'
                },
                "sort": [{"createdAt": "desc"}]
            }

            return  await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);
        } catch (e) {
            let message = `Error in getLastEvent function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }


    async getLastEventQueryPagination(ctx,experimentID, roundNumber, pageSize) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'events',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "desc"}]
            }

            return  await this.queryWithQueryStringWithPagination(ctx, JSON.stringify(queryString), pageSize);
        } catch (e) {
            let message = `Error in getLastEvent function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getLastEventQuery(ctx,experimentID, roundNumber) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'events',
                    "experimentID": experimentID,
                    "roundNo": roundNumber
                },
                "sort": [{"createdAt": "desc"}]
            }

            return  await this.queryWithQueryString(ctx, JSON.stringify(queryString));
        } catch (e) {
            let message = `Error in getLastEvent function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

    async getAllEvents(ctx) {
        try {
            let queryString = {
                "selector": {
                    "recordType": 'events'
                }
            }

            let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
            return queryResults;
        } catch (e) {
            let message = `Error in getLastEvent function: ${e}`
            Utils.dumpError(e);
            return shim.error(message)
        }
    }

}
module.exports = EventsContract