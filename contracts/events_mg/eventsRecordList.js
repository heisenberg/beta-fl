'use strict'

const StateList = require('./ledger-api/statelist.js')

const eventsRecord = require('./eventsRecord.js')

class EventsRecordList extends StateList {
    constructor(ctx) {
        super(ctx, 'org.foodsupplychain.eventsRecordList')
        this.use(eventsRecord)
    }

    async addRecord(record) {
        return this.addState(record)
    }

    async getRecord(key) {
        return this.getState(key)
    }

    async updateRecord(record) {
        return this.updateState(record)
    }


}

module.exports = EventsRecordList