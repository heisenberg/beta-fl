'use strict'
const State = require('./ledger-api/state.js')
var hasher = require('node-object-hash');
var hashSortCoerce = hasher({ sort: true, coerce: true });

class EventsRecord extends State {

    constructor(obj) {
        super(EventsRecord.getClass(), [obj.ID])
        Object.assign(this, obj)
    }


    getExperimentID() { return this.experimentID }
    getRoundNo() { return this.roundNo }
    getModelsLen() { return this.numberOfExamples }
    getCreatedAt() { return this.createdAt }
    getRecordType() { return this.recordType }
    getCounter()  {return this.counter}

    setRoundNo(roundNo) { this.roundNo = roundNo }
    setModelsLen(modelsLen) { this.modelsLen = modelsLen }
    setExperimentID(experimentID) { this.experimentID = experimentID }
    setCounter(counter) {this.counter = counter}


    static fromBuffer(buffer) {
        return EventsRecord.deserialize(Buffer.from(JSON.parse(buffer)))
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this))
    }

    static deserialize(data) {
        return State.deserializeClass(data, EventsRecord)
    }

    static createInstance( roundNo, experimentID, modelsLen, recordType) {

        let ID =  hashSortCoerce.hash( [roundNo, modelsLen, experimentID])
        let counter = 0
        let now = new Date()
        const createdAt = Math.round(now.getTime() / 1000)
        return new EventsRecord({ ID, roundNo, modelsLen,experimentID, recordType, counter, createdAt})
    }

    static getClass() {
        return 'org.foodsupplychain.eventsrecord'
    }

}

module.exports = EventsRecord